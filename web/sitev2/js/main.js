(function ($) {
    "use strict";
    
    // Initiate the wowjs
    new WOW().init();
    
    
    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });
    
    
    // Sticky Navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 90) {
            $('.nav-bar').addClass('nav-sticky');
            $('.carousel, .page-header').css("margin-top", "73px");
            $('.lien-cache').css("display", "block");
        } else {
            $('.nav-bar').removeClass('nav-sticky');
            $('.carousel, .page-header').css("margin-top", "0");
            $('.lien-cache').css("display", "none");
        }
    });
    
    
    // Dropdown on mouse hover
    $(document).ready(function () {
        function toggleNavbarMethod() {
            if ($(window).width() > 992) {
                $('.navbar .dropdown').on('mouseover', function () {
                    $('.dropdown-toggle', this).trigger('click');
                }).on('mouseout', function () {
                    $('.dropdown-toggle', this).trigger('click').blur();
                });
            } else {
                $('.navbar .dropdown').off('mouseover').off('mouseout');
            }
        }
        toggleNavbarMethod();
        $(window).resize(toggleNavbarMethod);
    });
    
    
    // jQuery counterUp
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 2000
    });
    
    
    /* Modal Video
    $(document).ready(function () {
        var $videoSrc;
        $('.btn-play').click(function () {
            $videoSrc = $(this).data("src");
        });
        console.log($videoSrc);

        $('#videoModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        })

        $('#videoModal').on('hide.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc);
        })
    });
    */


    // Testimonial Slider
    $('.testimonial-slider').slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.testimonial-slider-nav'
    });
    $('.testimonial-slider-nav').slick({
        arrows: false,
        dots: false,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: '22px',
        slidesToShow: 3,
        asNavFor: '.testimonial-slider'
    });
    $('.testimonial .slider-nav').css({"position": "relative", "height": "160px"});
    
    
    // Blogs carousel
    $(".related-slider").owlCarousel({
        autoplay: true,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            576:{
                items:1
            },
            768:{
                items:2
            }
        }
    });
    
    
    // Portfolio isotope and filter
    var portfolioIsotope = $('.portfolio-container').isotope({
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows'
    });

    $('#portfolio-flters li').on('click', function () {
        $("#portfolio-flters li").removeClass('filter-active');
        $(this).addClass('filter-active');

        portfolioIsotope.isotope({filter: $(this).data('filter')});
    });
    
})(jQuery);

function temps(){

    var mois = 4;
    var an = 52;
    var temps_connect = document.getElementById('temps_connexion');
    temps_connect.className = "btn btn-success";
    temps_connect.style = "font: bold 24px tahoma";

    console.log( $('#stage_user').val()/$('#heure_user').val() +'Semaines');

    if($('#stage_user').val()/$('#heure_user').val() < 1)
    {
        temps_connect.innerHTML =(Math.round($('#stage_user').val()/$('#heure_user').val())*7) +' Jour(s)';
    }
    if($('#stage_user').val()/$('#heure_user').val() < mois && $('#stage_user').val()/$('#heure_user').val() > 1)
    {
        temps_connect.innerHTML =Math.round($('#stage_user').val()/$('#heure_user').val()) +' Semaine(s)';
    }
    if( $('#stage_user').val()/$('#heure_user').val() > mois && $('#stage_user').val()/$('#heure_user').val() < an)
    {
        temps_connect.innerHTML = Math.round(($('#stage_user').val()/$('#heure_user').val())/mois) +' mois';
    }
    if( $('#stage_user').val()/$('#heure_user').val() > an)
    {
        temps_connect.innerHTML = Math.round(($('#stage_user').val()/$('#heure_user').val())/an) +' an(s)';
    }
}

 // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('temps2');
var img2 = document.getElementById('temps3') ;
var modalImg = document.getElementById("img01");
var modalanswer = document.getElementById("answer");

/*
img.onclick = function(){

    modal.style.display = "block";
    modalImg.src = this.src;

};
img2.onclick = function(){

    modal.style.display = "block";
    modalImg.src = this.src;

};
*/

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {

    modal.style.display = "none";

};

function navigation(){ 
    lien = window.location.href;

    if(lien.includes("niveau_de_langue")){
        $(".active").removeClass('active');
        $(".niv-lang").addClass('active');
    }
    if(lien.includes("pour_qui")){
        $(".active").removeClass('active');
        $(".pour_qui").addClass('active');
    }
    if(lien.includes("notre_methode") || lien.includes("assurer_la_qualite")){
        $(".active").removeClass('active');
        $(".method").addClass('active');
    }
    if(lien.includes("comment_s_inscrire") || lien.includes("comprendre_la_plateforme")){
        $(".active").removeClass('active');
        $(".inscrire").addClass('active');
    }
    if(lien.includes("nos_prix")){
        $(".active").removeClass('active');
        $(".nos_prix").addClass('active');
    }       
}


navigation();

window.onscroll = function() {myFunction()};

var header = document.getElementById("top-bar-ubuntu");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("mon-sticky");
  }else {
    header.classList.remove("mon-sticky");
  }
}