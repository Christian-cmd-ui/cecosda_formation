// @author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
// @creation_date : 24/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);
    var loadingOverlay = document.querySelector('.loading-overlay');
    if (loadingOverlay) {
        console.log('loading over');
        head.load([
            APP_ROOT + '/assets/js/library/loadingoverlay.min.js'
        ]);
    }

    var datatable = document.querySelector('table.datatable');
    if (datatable) {
        head.load([
            "/assets/js/jquery.dataTables.min.js",
            "/assets/js/dataTables.bootstrap4.min.js"
        ], function () {
            $(".datatable").DataTable(
                {
                    'language': {
                        'sProcessing': 'Traitement en cours...',
                        'sSearch': 'Rechercher&nbsp;:',
                        'sLengthMenu': 'Afficher _MENU_ &eacute;l&eacute;ments',
                        'sInfo': 'Affichage de l\'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments',
                        'sInfoEmpty': 'Affichage de l\'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment',
                        'sInfoFiltered': '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
                        'sInfoPostFix': '',
                        'sLoadingRecords': 'Chargement en cours...',
                        'sZeroRecords': 'Aucun &eacute;l&eacute;ment &agrave; afficher',
                        'sEmptyTable': 'Aucune donn&eacute;e disponible dans le tableau',
                        'oPaginate': {
                            'sFirst': 'Premier',
                            'sPrevious': 'Pr&eacute;c&eacute;dent',
                            'sNext': 'Suivant',
                            'sLast': 'Dernier'
                        },
                        'oAria': {
                            'sSortAscending': ': activer pour trier la colonne par ordre croissant',
                            'sSortDescending': ': activer pour trier la colonne par ordre d&eacute;croissant'
                        }
                    }
                }
            );
        });
    }
});