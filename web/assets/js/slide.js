    
    var show_per_page = 1;
    var current_page = 0;
    var number_of_pages = Math.ceil($('#content').children().length / show_per_page);
    function set_display(first, last) {
        $('#content').children().css('display', 'none');
        $('#content').children().slice(first, last).css('display', 'block');
    }

    function previous(){
        if($('.active').prev('.page_link').length) go_to_page(current_page - 1);
    }

    function next(){
        
        if($('.active').next('.page_link').length) go_to_page(current_page + 1);
    }

    function go_to_page(page_num){
      
        current_page = page_num;
        start_from = current_page * show_per_page;
        end_on = start_from + show_per_page;
        set_display(start_from, end_on);
        $('.active').removeClass('active');
        $('#id' + page_num).addClass('active');
    }

    $(document).ready(function() {
        let j = 0;
        let titrecours = $('.mySlides h3');
        console.log(titrecours.length);
        while( j < titrecours.length){
            let li = document.createElement('li');
            li.style.float = 'left';
            li.style.listStyle = 'none';
            li.style.textDecoration = 'none';
            li.style.position = 'relative';

            let a = document.createElement('a');
            a.href= 'javascript:go_to_page('+j+')';
            a.innerText = titrecours[j].textContent;
            li.appendChild(a);
            $('#cours').append(li);
            j++;
        }

       
        var nav = '<ul class="pagination "><li><a href="javascript:previous();">&laquo;</a>';

        var i = -1;
        while(number_of_pages > ++i){
            nav += '<li class="page_link'
            if(!i) nav += ' active';
            nav += '" id="id' + i +'">';
            nav += '<a class="breadcrumb" href="javascript:go_to_page(' + i +')">'+ (i + 1) +'</a>';
        }
        nav += '<li><a href="javascript:next();">&raquo;</a></ul>';

        $('#page_navigation').html(nav);
        set_display(0, show_per_page);

        
      
            
           
       
    });

var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    
    slides[slideIndex-1].style.display = "block";
    
    setTimeout(showSlides, 8000); // Change image every 8 seconds
    next();
}


var slideIndex2 = 0;
showSlide();

function showSlide() {
    var i;
    var slide = document.getElementsByClassName("slide");
    
    for (i = 0; i < slide.length; i++) {
        slide[i].style.display = "none";
    }
    slideIndex2++;
    if (slideIndex2 > slide.length) {slideIndex2 = 1}
        slide[slideIndex2-1].className = "slide fadeIn";
        slide[slideIndex2-1].style.display = "block";

    setTimeout(showSlide, 8000); // Change image every 8 seconds
    next();
}