//@author : Kongnuy for CECOSDA Formation
//@creationDate : 24/09/2021

$(function () {
    var APP_ROOT;
  
    window.location.pathname.match(/(.*?)web/i)
      ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1])
      : (APP_ROOT = "");
    APP_ROOT ? (APP_ROOT += "web") : APP_ROOT;
  
    head.load([APP_ROOT + "/assets/js/FeedbackHelper.js"], function () {
      /**
       * constructor
       */
      function Article() {
        this.feedbackHelper = new FeedbackHelper();
      }
  
      var article = new Article();
  
      /**
       * allow to initialize the view
       * @return {void} nothing
       */
      Article.prototype.initializeView = function () {};
  
      /**
       * allow to set a whole bunch of listeners
       */
      Article.prototype.setListeners = function () {
        this.validateArticleListener();
      };
  
      Article.prototype.validateArticleListener = function () {
        $(".ajax-Article-delete").on("click", function (e) {
          e.preventDefault();
  
          var link = $(this);
          var feedbackMessage = JSON.parse(
            JSON.stringify({
              title: "Suppression d'un article",
              message:
                "En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible",
              type: "warning",
              confirmeButtonText: "Je confirme",
            })
          );
          var callback = function () {
            $.ajax({
              headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
              },
              method: "DELETE",
              url: link.data("url"),
              success: function (response) {
                var res;
                if (typeof response === "object") {
                  res = response;
                } else {
                  try {
                    res = JSON.parse(response);
                  } catch (error) {
                    console.error(error);
                    res = response;
                  }
                }
  
                if (res.status === "success") {
                  var feedback = JSON.parse(
                    JSON.stringify({
                      title: "Suppression d'un article",
                      message: "L'article a été supprimé",
                      type: "success",
                    })
                  );
                  location.reload();
                  article.feedbackHelper.showAutoCloseMessage(
                    feedback.title,
                    feedback.message,
                    feedback.type
                  );
                } else {
                  var feedbackError = JSON.parse(
                    JSON.stringify({
                      title: "Suppression Impossible",
                      message: "La suppression a echoué",
                      type: "error",
                    })
                  );
                  article.feedbackHelper.showAutoCloseMessage(
                    feedbackError.title,
                    res.message,
                    feedbackError.type
                  );
                }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
              },
            });
          };
          article.feedbackHelper.showLoaderMessage(
            feedbackMessage.title,
            feedbackMessage.message,
            feedbackMessage.type,
            feedbackMessage.confirmeButtonText,
            callback
          );
        });
      };
  
      //this should be at the end
      article.initializeView();
      article.setListeners();
    });
  });
  