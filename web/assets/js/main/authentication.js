// @author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
// @creation_date : 11/04/2021

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);
    var message = document.querySelector('.hidden_message');
    if (message) {
        Lobibox.notify('warning', {
            title: 'anglaisenligne.cm',
           // soundPath: '../assets/plugins/notifications/sounds/',
            img: '../assets/img/LogoCL.jpg',
            pauseDelayOnHover: true,
            continueDelayOnInactiveTab: false,
            rounded : true,
            position: 'top center',
            delay: false,
            width: 600,
            showClass: 'fadeInDown',                // Show animation class
            hideClass: 'zoomOut',
            msg: $('#hidden_flash_bag_message').val()
        });
    }
});