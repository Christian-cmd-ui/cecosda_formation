
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function FeedbackDevoir() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var feedback_devoir = new FeedbackDevoir();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        FeedbackDevoir.prototype.initializeView = function() {
            var noteAutoSur30 = parseFloat($('#note_correction_auto').val());
            var noteAudio = 0;
            var noteSaisie = 0;
            $('input.notation').each(function(){
                var value = parseFloat($(this).val());
                if (!isNaN(value) && value > 0){
                    if ($(this).data('type') === 'saisie'){
                        noteSaisie += value;
                    }else {
                        noteAudio += value;
                    }
                }
            });
            var totalAudio = $('#total_audio_value').val();
            var totalSaisie = $('#total_saisie').val();
            var noteAudioSur30 = (noteAudio * 30)/totalAudio;
            var noteSaisieSur30 = (noteSaisie * 30)/totalSaisie;
            var noteFinale = (noteAutoSur30 + noteAudioSur30 + noteSaisieSur30)/3;
            $('#app_bundle_devoir_noteDevoir').val(noteFinale);
            $('#hidden_note_correction').val(noteFinale);
        };

        /**
         * allow to set a whole bunch of listeners
         */
        FeedbackDevoir.prototype.setListeners = function() {
            this.submitFeedbackDevoirOnceFirstListener();
            this.devoirRubriqueSubmitListener();
            this.inputListener();
        };

        FeedbackDevoir.prototype.devoirRubriqueSubmitListener = function(){
            $('.exercice-ajax-tmp-save').on('click', function (e) {
                e.preventDefault();

                var link = $(this);
                var idExercice = link.data('id');
                var max = parseFloat(link.data('max'));
                var remarqueId = parseInt($('#feedback-remark-' + idExercice).val());
                var noteExercice = parseFloat($('#note-' + idExercice).val());
                var editor = 'feedback-comment-' + idExercice;
                var comment  = CKEDITOR.instances[editor].getData();
                var devoir = parseInt($('#devoir_id').val());
                if (isNaN(noteExercice) || noteExercice < 0){
                    var feedbackNote = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Bien vouloir renseigner la note de l'exercice avant d'enregistrer",
                        'type' : 'warning'
                    }));
                    feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackNote.title, feedbackNote.message, feedbackNote.type);
                    return;
                }

                if (max < noteExercice){
                    var feedbackMax = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "La note doit être inférieure ou égale à la note de l'exercice",
                        'type' : 'warning'
                    }));
                    feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackMax.title, feedbackMax.message, feedbackMax.type);
                    return;
                }

                if (remarqueId === 0){
                    var feedbackRemark = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Bien vouloir renseigner la remarque de cet exercice",
                        'type' : 'warning'
                    }));
                    feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackRemark.title, feedbackRemark.message, feedbackRemark.type);
                    return;
                }
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : " Êtes vous sûr de vouloir sauvegarder temporairement le feedback de cette rubrique ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var data = JSON.parse(JSON.stringify({
                        'exercice': idExercice,
                        'remarque': remarqueId,
                        'comment': comment,
                        'note': noteExercice,
                        'devoir': devoir
                    }));
                    //2. Post avec AJAX
                    $.ajax({
                        type: "POST",
                        url: link.data('url'),
                        data: {data: data},
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Feedback Par rubrique",
                                    'message': "Le feedback de cette rubrique  a été enregistré avec succès. Vous pouvez continuer avec votre correction",
                                    'type': 'success'
                                }));
                                feedback_devoir.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                                var $row = link.parent().parent().parent();
                                if (!$row.hasClass('save-bloc')){
                                    $row.addClass('save-bloc');
                                }
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': "Erreur",
                                    'message': "Impossible de soumettre cette partie du feedback, Si le problème persiste, Bien vouloir contacter le service client de anglaisenligne.cm",
                                    'type': 'error'
                                }));
                                feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                feedback_devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        FeedbackDevoir.prototype.submitFeedbackDevoirOnceFirstListener = function(){
            $('input[name="save-feedback"]').on('click', function (e) {
                e.preventDefault();
                var counter = null;
                $('input.notation').each(function(index){
                    var value = parseFloat($(this).val());
                    if (isNaN(value) || value < 0){
                        counter = index;
                        return false;
                    }else {
                        var maxValue = parseFloat($(this).data('max'));
                        if ($(this).data('type') === 'saisie'){
                            if (value > maxValue){
                                counter = index;
                                return false;
                            }
                        }else {
                            if (value > 3){
                                counter = index;
                                return false;
                            }
                        }
                    }
                });
                if (counter !== null){
                    var feedbackAudio = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "La note de l'exercice " + (counter + 1) + " incorrecte. Bien vouloir vérifier avant de soumettre votre feedback",
                        'type' : 'warning'
                    }));
                    feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackAudio.title, feedbackAudio.message, feedbackAudio.type);
                    return;
                }

                var exercice = $('input[name="count_exercices"]').val();
                for (var i = 1; i <= exercice; i++) {
                    var exerciceId = $('#hidden_exercise_id_' + i).val();
                    var value = parseInt($('#feedback-remark-'+exerciceId).val());
                     if (value === 0){
                         var feedbackWarning = JSON.parse(JSON.stringify({
                         'title' : "anglaisenligne.cm",
                         'message' : "Bien vouloir renseigner la remarque de la rubrique "+i,
                         'type' : 'warning'
                         }));
                         feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                         return;
                     }
                }

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir soumettre le feedback du devoir ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $('form[name="app_bundle_devoir"]').submit();
                };
                feedback_devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        FeedbackDevoir.prototype.inputListener = function(){
            $('.notation').on('input', function () {
                var value = parseFloat($(this).val());
                if (!isNaN(value) && value >= 0){
                    var maxValue = parseFloat($(this).data('max'));
                    if ($(this).data('type') === 'saisie'){
                        if (value > maxValue){
                            var feedbackSaisie = JSON.parse(JSON.stringify({
                                'title' : "anglaisenligne.cm",
                                'message' : "La note doit être inférieure ou égale au nombre de questions de cet exercice.",
                                'type' : 'warning'
                            }));
                            feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackSaisie.title, feedbackSaisie.message, feedbackSaisie.type);
                            return;
                        }
                    }else {
                       if (value > 3){
                           var feedbackWarning = JSON.parse(JSON.stringify({
                               'title' : "anglaisenligne.cm",
                               'message' : "La note doit être inférieure ou égale 3.",
                               'type' : 'warning'
                           }));
                           feedback_devoir.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                           return;
                       }
                    }
                    feedback_devoir.calculateTotalMarks();
                }
            });
        };
        FeedbackDevoir.prototype.calculateTotalMarks = function () {
            var noteAutoSur30 = parseFloat($('#note_correction_auto').val());
            var noteAudio = 0;
            var noteSaisie = 0;
            $('input.notation').each(function(){
                var value = parseFloat($(this).val());
                if (!isNaN(value) && value >= 0){
                    if ($(this).data('type') === 'saisie'){
                        noteSaisie += value;
                    }else {
                        noteAudio += value;
                    }
                }
            });
            var totalAudio = parseFloat($('#total_audio_value').val());
            var totalSaisie = parseFloat($('#total_saisie').val());
            var noteAudioSur30 = (noteAudio * 30)/totalAudio;
            var noteSaisieSur30 = (noteSaisie * 30)/totalSaisie;
            var noteFinale = (noteAutoSur30 + noteAudioSur30 + noteSaisieSur30)/3;
            $('#app_bundle_devoir_noteDevoir').val(noteFinale);
            $('#hidden_note_correction').val(noteFinale);
        };

        //this should be at the end
        feedback_devoir.initializeView();
        feedback_devoir.setListeners();
    });
});