
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 04/04/2021

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Palier() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var palier = new Palier();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Palier.prototype.initializeView = function() {};

        /**
         * allow to set a whole bunch of listeners
         */
        Palier.prototype.setListeners = function() {
            this.chooseNiveauListener();
        };

        Palier.prototype.chooseNiveauListener = function(){
            $('.ajax-niveau-choose').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Êtes-vous sûr de vouloir souscrire au niveau " + link.data('value')+"?",
                    'message' : "",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': 'Le niveau a été choisi avec succès, Voulez-vous commencer votre formation maintenant?',
                                    'type': 'success',
                                    'confirmeButtonText' : 'Je le confirme'
                                }));
                                var callback1 = function () {
                                    window.location.href = APP_ROOT + '/test/student/training/'+response.id+'/in/process/0/course';
                                };
                                palier.feedbackHelper.showLoaderMessage(feedback.title, feedback.message, feedback.type, feedbackMessage.confirmeButtonText, callback1)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'anglaisenligne.cm',
                                    'message': "Le choix du niveau a echoué",
                                    'type': 'error'
                                }));
                                palier.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                palier.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };
        //this should be at the end
        palier.initializeView();
        palier.setListeners();
    });
});