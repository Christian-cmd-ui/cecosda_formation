// @author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
// @email: shydilaicard@gmail.com
// @creation_date : 04/04/2021

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    var intPhoneNumber = document.querySelector('#app_bundle_rejoindre_telephone');
    if (intPhoneNumber) {
        head.load([
            APP_ROOT + '/assets/js/library/intlTelInput.js'
        ], function () {
            var inputMain = document.querySelector("#app_bundle_rejoindre_telephone");
            window.intlTelInput(inputMain, {
                autoHideDialCode: false,
                nationalMode: false,
                separateDialCode: true,
                preferredCountries: ['cm', 'ci'],
                placeholderNumberType: "MOBILE",
                initialCountry : 'CM'
            });
        });
    }

    var intUserPhoneNumber = document.querySelector('#app_bundle_rejoindre_telephone');
    if (intUserPhoneNumber) {
        head.load([
            APP_ROOT + '/assets/js/library/intlTelInput.js'
        ], function () {
            var inputMain = document.querySelector("#app_bundle_rejoindre_telephone");
            window.intlTelInput(inputMain, {
                autoHideDialCode: false,
                nationalMode: false,
                separateDialCode: true,
                preferredCountries: ['cm', 'ci'],
                placeholderNumberType: "MOBILE",
                initialCountry : 'CM'
            });
        });
    }
});