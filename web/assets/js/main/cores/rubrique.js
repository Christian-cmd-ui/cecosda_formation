
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 18/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Rubrique() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var rubrique = new Rubrique();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Rubrique.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Rubrique.prototype.setListeners = function() {
            this.validateRubriqueListener();
        };

        Rubrique.prototype.validateRubriqueListener = function(){
            $('.ajax-rubrique-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'une rubrique',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Suppression d'un rubrique",
                                    'message': 'La rubrique a été supprimé',
                                    'type': 'success'
                                }));
                                location.reload();
                                rubrique.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                rubrique.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                rubrique.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        //this should be at the end
        rubrique.initializeView();
        rubrique.setListeners();

    });
});