
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Reservation() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var reservation = new Reservation();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Reservation.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Reservation.prototype.setListeners = function() {
            this.validateReservationListener();
        };

        Reservation.prototype.validateReservationListener = function(){
            $('.ajax-reservation-cancel').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Annulation d\'une reservation',
                    'message' : 'En confirmant, vous certifiez que vous annuler votre reservation, voulez vous continuer ?\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Annuler d'une reservation",
                                    'message': 'Le reservation a été annulée',
                                    'type': 'success'
                                }));
                                location.reload();
                                reservation.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Annulation Impossible',
                                    'message': "L'annulation a echouée",
                                    'type': 'error'
                                }));
                                reservation.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                reservation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        //this should be at the end
        reservation.initializeView();
        reservation.setListeners();
    });
});