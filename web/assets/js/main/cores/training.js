
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Training() {
            this.feedbackHelper = new FeedbackHelper();
            this.tabValue4 = 0;
        }

        var training = new Training();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Training.prototype.initializeView = function() {};

        /**
         * allow to set a whole bunch of listeners
         */
        Training.prototype.setListeners = function() {
            this.palierSelectorListener();
            this.niveauSelectorListener();
            this.showCorrection_1Listener();
            this.showCorrection_2Listener();
            this.showCorrection_3Listener();
            this.showCorrection_4Listener();
            this.showCorrection_5Listener();
            this.tabButtonListener();
            this.nextButtonListener();
            this.devoirButtonListener();
        };


        Training.prototype.palierSelectorListener = function() {
            $("select[name='palier_selection']").on('change', function () {
                var that = this;
                if (parseInt($(this).val()) === 0){
                    return;
                }
                var urlPrix = this.options[this.selectedIndex].getAttribute('prix');
                var urlDesc = this.options[this.selectedIndex].getAttribute('desc');
                $(that).parent().LoadingOverlay('show');
                $('#amount').load(urlPrix, function () {
                });
                $('#description').load(urlDesc, function () {
                    $(that).parent().LoadingOverlay('hide')
                })
            })
        };

        Training.prototype.niveauSelectorListener = function() {
            $("select[name='niveau_selection']").on('change', function () {
                var that = this;
                if (parseInt($(this).val()) === 0){
                    return;
                }
                var urlPrix = this.options[this.selectedIndex].getAttribute('prix');
                var urlDesc = this.options[this.selectedIndex].getAttribute('desc');
                $(that).parent().LoadingOverlay('show');
                $('#amount-niveau').load(urlPrix, function () {
                });
                $('#description-niveau').load(urlDesc, function () {
                    $(that).parent().LoadingOverlay('hide')
                })
            })
        };

        Training.prototype.showCorrection_1Listener = function() {
            $("button[name='display-correction_1']").on('click', function () {
               $('div.exercice-display_1').show();
            })
        };

        Training.prototype.showCorrection_2Listener = function() {
            $("button[name='display-correction_2']").on('click', function () {
               $('div.exercice-display_2').show();
            })
        };

        Training.prototype.showCorrection_3Listener = function() {
            $("button[name='display-correction_3']").on('click', function () {
               $('div.exercice-display_3').show();
            })
        };
        Training.prototype.showCorrection_4Listener = function() {
            $("button[name='display-correction_4']").on('click', function () {
               $('div.exercice-display_4').show();
            })
        };

        Training.prototype.showCorrection_5Listener = function() {
            $("button[name='display-correction_5']").on('click', function () {
               $('div.exercice-display_5').show();
            })
        };

        Training.prototype.tabButtonListener = function() {
            $("body").on('click',"#exercise", function () {
                training.tabValue4++;
                if (training.tabValue4 > 0)
                {
                    $("#next-course1").show();
                    $("#next-course2").show();
                    $("#test_now1").attr('data-toggle', 'modal');
                    $("#test_now2").attr('data-toggle', 'modal');
                    $("#test_later1").show();
                    $("#test_later2").show();
                }
            })
        };

        Training.prototype.nextButtonListener = function() {
            $("body").on('click',".next_course", function () {
                if (training.tabValue4 > 0) {
                    var link = $(this).data('url');
                    window.location.href = APP_ROOT + link;
                }else {
                    var feedbackWarning = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Vous devez parcourir toutes les parties(Cours, Exercice) avant de passer au cours suivant. Cliquer sur la partie correspondante pour visualiser le contenu.",
                        'type' : 'warning'
                    }));
                    training.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                    return;
                }
            })
        };

        Training.prototype.devoirButtonListener = function() {
            $("body").on('click',".devoir", function () {
                if (training.tabValue4 === 0) {
                    var feedbackWarning = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Vous devez parcourir toutes les parties(Cours, Exercice) avant de faire le devoir. Cliquer sur la partie correspondante pour visualiser le contenu.",
                        'type' : 'warning'
                    }));
                    training.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                    return;
                }
            })
        };

        //this should be at the end
        training.initializeView();
        training.setListeners();
    });
});