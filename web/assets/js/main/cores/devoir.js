
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Devoir() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var devoir = new Devoir();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Devoir.prototype.initializeView = function() {};

        /**
         * allow to set a whole bunch of listeners
         */
        Devoir.prototype.setListeners = function() {
            this.submitDevoirOnceFirstListener();
            this.submitDevoirToBeContinueFirstListener();
            this.submitDevoirContinue2Listener();
            this.submitDevoirOnce2Listener();
        };

        Devoir.prototype.submitDevoirToBeContinueFirstListener = function(){
            $('input[name="saveAndContinue"]').on('click', function (e) {
                e.preventDefault();

                /*var counter = parseInt($('input#counter_audio').val());// Recupère le nombre d'audio du devoir
                var audioCounter = 0;
                for (var i = 1; i <= counter; i++) {
                    var audio = $('#audio_path_' + i).val();
                    if (audio !== ''){
                        audioCounter += 1;
                    }
                }*/

                /*if (audioCounter !== counter){
                    var remainCounter = counter - audioCounter;
                    var feedbackWarning = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Bien vouloir enregistrer tous les audios avant de soumettre votre devoir. \n Il vous reste "+ remainCounter + " audios non enregistrés",
                        'type' : 'warning'
                    }));
                    devoir.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                    return;
                }*/

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Êtes-vous sûr de vouloir soumettre le devoir ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "hidden_input").val(0);
                    var $form = $('#devoir_form');
                    $form.append(input);
                    $form.submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Devoir.prototype.submitDevoirOnceFirstListener = function(){
            $('input[name="save"]').on('click', function (e) {
                e.preventDefault();
                var counter = parseInt($('input#counter_audio').val());// Recupère le nombre d'audio du devoir
                var audioCounter = 0;
                for (var i = 1; i <= counter; i++) {
                    var audio = $('#audio_path_' + i).val();
                    if (audio !== ''){
                        audioCounter += 1;
                    }
                }
                if (audioCounter !== counter){
                    var remainCounter = counter - audioCounter;
                    var feedbackWarning = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Bien vouloir enregistrer tous les audios avant de soumettre votre devoir. \n Il vous reste "+ remainCounter + " audios non enregistrés",
                        'type' : 'warning'
                    }));
                    devoir.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                    return;
                }

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir soumettre le devoir ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "hidden_input").val(1);
                    var $form = $('#devoir_form');
                    $form.append(input);
                    $form.submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };

        Devoir.prototype.submitDevoirContinue2Listener = function(){
            $('input[name="saveAndContinue2"]').on('click', function (e) {
                e.preventDefault();

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir enregistrer ce devoir et continuer plus tard ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "hidden_input").val(0);
                    var $form = $('#devoir_form');
                    $form.append(input);
                    $form.submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Devoir.prototype.submitDevoirOnce2Listener = function(){
            $('input[name="save2"]').on('click', function (e) {
                e.preventDefault();
                var counter = parseInt($('input#counter_audio').val());// Recupère le nombre d'audio du devoir
                var audioCounter = 0;
                for (var i = 1; i <= counter; i++) {
                    var audio = $('#audio_path_' + i).val();
                    if (audio !== ''){
                        audioCounter += 1;
                    }
                }
                if (audioCounter !== counter){
                    var remainCounter = counter - audioCounter;
                    var feedbackWarning = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Bien vouloir enregistrer tous les audios avant de soumettre votre devoir. \n Il vous reste "+ remainCounter + " audios non enregistrés",
                        'type' : 'warning'
                    }));
                    devoir.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type);
                    return;
                }
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir soumettre ce devoir ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "hidden_input").val(1);
                    var $form = $('#devoir_form');
                    $form.append(input);
                    $form.submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        //this should be at the end
        devoir.initializeView();
        devoir.setListeners();
    });
});