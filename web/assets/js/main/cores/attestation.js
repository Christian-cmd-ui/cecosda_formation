
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Attestation() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var attestation = new Attestation();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Attestation.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Attestation.prototype.setListeners = function() {
            this.confirmAttestationListener();
        };

        Attestation.prototype.confirmAttestationListener = function(){
            $('.ajax-attestation-confirm').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir confimer la lévée de l'anonymat de cet apprenant",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Succès",
                                    'message': "La confirmation s'est effectué avec succès. Un mail a été envoyé à l'apprenant pour le remplissage des informations qui vont apparaitre dans son attestion",
                                    'type': 'success'
                                }));
                                attestation.feedbackHelper.showMessageWithPromptCallBack(feedback.title, feedback.message, feedback.type, function () {
                                    location.reload();
                                })
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Erreur',
                                    'message': "Une erreur est survenue",
                                    'type': 'error'
                                }));
                                attestation.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                attestation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        //this should be at the end
        attestation.initializeView();
        attestation.setListeners();
    });
});