
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Register() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var register = new Register();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Register.prototype.initializeView = function() {};

        /**
         * allow to set a whole bunch of listeners
         */
        Register.prototype.setListeners = function() {
            this.categorieSelectorListener();
        };

        Register.prototype.categorieSelectorListener = function() {
            $("select#fos_user_registration_form_categorie").on('change', function () {
                if (parseInt($(this).val()) !== 3){
                    $('div.anonyme_block').hide();
                }else {
                    $('div.anonyme_block').show();
                }
            })
        };

        //this should be at the end
        register.initializeView();
        register.setListeners();
    });
});