
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Trou() {
            this.feedbackHelper = new FeedbackHelper();
            this.question_number = 0;
            this.id = null;
            this.exercise = null;
            this.questions = {};
            this.errors = 0;
            this.attributesArray = {} // has to be serialized so we have this form
        }

        var trou = new Trou();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Trou.prototype.initializeView = function() {
        };

        Trou.prototype.postActions  = function () {
            var exerciseQuestions = document.querySelector('.exercise-questions');
            if (exerciseQuestions) {
                $('#addQuestion').trigger('click')
            }
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Trou.prototype.setListeners = function() {
            this.validateTrouListener();
        };

        Trou.prototype.validateTrouListener = function(){
            $('.ajax-trou-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'un trou',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Suppression d'un trou",
                                    'message': 'Le trou a été supprimé',
                                    'type': 'success'
                                }));
                                location.reload();
                                trou.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                trou.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                trou.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };

        Trou.prototype.addQuestionListener  = function() {
            $('#addQuestion').on('click', function (e) {
                e.preventDefault();
                var questionForm = $('div.template.question_form').clone().removeClass('template');
                questionForm.find('legend b').text(++(trou.question_number));
                questionForm.appendTo('div#question div.question_form_container').slideDown('slow')
            })
        };

        Trou.prototype.removeQuestionListener = function () {
            $('#removeQuestion').on('click', function (e) {
                e.preventDefault();
                // Verifier si on n'a pas atteint le minimun
                if (trou.question_number > 1) {
                    --trou.question_number;
                    $('div.question_form').not('.template').last().remove().slideUp('slow')
                } else {
                    trou.feedbackHelper.showMessageWithPrompt('Désolé', 'Vous devez ajouter au minimun un cours', 'warning')
                }
            })// fin
        };

        Trou.prototype.getValuesFromForm =  function () {
            this.exercise = parseInt($('input#exercice').val());
            this.slug = $('input#exercice-slug').val();
            // get the questions values
            $('.question_form').not('.template').each(function (index) {
                var element = {};
                trou.errors = 0;
                element['name'] = $(this).find('input#app_bundle_trou_nom').val();
                if(!element['name'] || element['name'].trim().length == 0){
                    ++trou.errors;
                }
                element['test'] = parseInt($(this).find('select#app_bundle_trou_test').val());
                trou.questions[index] = element
            })
        };

        /**
         * Returns a JSON representation of the object Course
         * @return {JSON}
         */
        Trou.prototype.getJSONValues = function() {
            this.getValuesFromForm();
            this.attributesArray['exercise'] = this.exercise;
            this.attributesArray['questions'] = this.questions;
            return JSON.parse(JSON.stringify(this.attributesArray))
        };

        Trou.prototype.saveQuestionsListener = function () {
            $('#saveQuestions').on('click', function (e) {
                e.preventDefault();
                var data = trou.getJSONValues();
                //console.log(data);
                var link = $('input#exercice-link').val();
                var redirect = $('input#redirect-link').val();
                if (trou.errors > 0){
                    trou.feedbackHelper.showMessageWithPrompt('Désolé', 'Tous les champs du formulaire ne sont pas renseignés', 'error')
                    return;
                }
                $.ajax({
                    method: 'POST',
                    data: {data: data},
                    url: link,
                    dataType: 'JSON',
                    success: function (returnedData) {
                        console.log(returnedData);
                        if (returnedData.status === 'success') {
                            var callback = function() {
                                window.location.href = redirect
                            };
                            trou.feedbackHelper.showMessageWithPromptCallBack('Enregistrement réussi', 'Les trous  ont été enregistrés avec succès', 'success', callback)
                        } else {
                            trou.feedbackHelper.showMessageWithPrompt('Désolé', returnedData.message, 'error')
                        }
                    },
                    error: function (returnedData) {
                        console.error(returnedData);
                        trou.feedbackHelper.showMessageWithPrompt('Désolé', 'Un problème est survenu lors de l\'enregistrement, s\'il persiste contacter l\'administrateur', 'error')
                    }
                })
            })
        };


        //this should be at the end
        trou.initializeView();
        trou.setListeners();
        trou.addQuestionListener();
        trou.removeQuestionListener();
        trou.saveQuestionsListener();
        trou.postActions();
    });
});