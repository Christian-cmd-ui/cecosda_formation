
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function FeedbackEvaluation() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var feedback_evaluation = new FeedbackEvaluation();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        FeedbackEvaluation.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        FeedbackEvaluation.prototype.setListeners = function() {
            this.chooseNomListener();//1
            this.choosePronomsListener();//2
            this.chooseVerbesListener();//3
            this.chooseAdjectifListener();//4
            this.chooseAdverbesListener();//5
            this.choosePrepositionListener();//6
            this.chooseConjonctionListener();//7
            this.chooseDeterminantListener();//8
            this.chooseStructurePhrasesListener();//9
            this.chooseCalendrierListener();//10
            this.chooseCultureListener();//11
            this.chooseRepetitionOraleListener();//12
            this.chooseDicteeListener();//13
            this.chooseTraductionTexteListener();//14
            this.chooseRedactionGrammaireListener();//15
            this.chooseRedactionLibreListener();//16
            this.chooseComprehensionOralListener();//17
            this.chooseComprehensionEcritListener();//18
            this.chooseVocabulairesListener();//19
            this.chooseLectureListener();//20
            this.chooseExpressionLibreListener();//21


            this.submitEvaluationListener();
        };

        FeedbackEvaluation.prototype.submitEvaluationListener = function(){
            $('input[name="save-feedback"]').on('click', function (e) {
                e.preventDefault();
                var noteDevoir = parseInt($('input#note_finale_devoir').val());// Recupère le note finale du devoir
                var noteEvaluation = parseInt($('input#note_finale_evaluation').val());// Recupère le note finale de l'evaluation

                var total = noteDevoir + noteEvaluation;
                if (total < 60){
                    var feedbackMessage1 = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "La note finale de ce niveau est inférieure à 60 qui est inférieure à la note exigible pour avoir l'attestation de fin de niveau\n, Voulez-vous quand même soumettre le feedback de cette évaluation ? ",
                        'type' : 'warning',
                        'confirmeButtonText' : 'Je le confirme'
                    }));
                    var callback1 = function () {
                        var input = $("<input>").attr("type", "hidden").attr("name", "hidden_input_note_finale").val(total);
                        var $form = $('form[name="app_bundle_evaluation"]');
                        $form.append(input);
                        $form.submit();
                    };
                    feedback_evaluation.feedbackHelper.showLoaderMessage(feedbackMessage1.title, feedbackMessage1.message, feedbackMessage1.type, feedbackMessage1.confirmeButtonText, callback1);
                }else {
                    var feedbackMessage = JSON.parse(JSON.stringify({
                        'title' : "anglaisenligne.cm",
                        'message' : "Etes-vous sûr de vouloir soumettre le feedback de l'évaluation ?",
                        'type' : 'warning',
                        'confirmeButtonText' : 'Je le confirme'
                    }));
                    var callback = function () {
                        var input = $("<input>").attr("type", "hidden").attr("name", "hidden_input_note_finale").val(total);
                        var $form = $('form[name="app_bundle_evaluation"]');
                        $form.append(input);
                        $form.submit();
                    };
                    feedback_evaluation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
                }
            });
        };

        FeedbackEvaluation.prototype.chooseNomListener = function(){// Noms 1
            $('.feedback_evaluation_noms').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_noms').val()) + 1); i++) {
                    var value = parseFloat($('select#noms_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_noms').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.choosePronomsListener = function(){// Pronoms 2
            $('.feedback_evaluation_pronoms').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_pronoms').val()) + 1); i++) {
                    var value = parseFloat($('select#pronoms_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_pronoms').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseVerbesListener = function(){// Verbes 3
            $('.feedback_evaluation_verbes').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_verbes').val()) + 1); i++) {
                    var value = parseFloat($('select#verbes_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_verbes').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseAdjectifListener = function(){// Adjectif 4
            $('.feedback_evaluation_adjectifs').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_adjectifs').val()) + 1); i++) {
                    var value = parseFloat($('select#adjectifs_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_adjectifs').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseAdverbesListener = function(){// Adverbes 5
            $('.feedback_evaluation_adverbes').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_adverbes').val()) + 1); i++) {
                    var value = parseFloat($('select#adverbes_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_adverbes').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.choosePrepositionListener = function(){// Prepositions 6
            $('.feedback_evaluation_prepositions').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_prepositions').val()) + 1); i++) {
                    var value = parseFloat($('select#prepositions_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_prepositions').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseConjonctionListener = function(){// Conjonctions 7
            $('.feedback_evaluation_conjonctions').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_conjonctions').val()) + 1); i++) {
                    var value = parseFloat($('select#conjonctions_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_conjonctions').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseDeterminantListener = function(){// Determinants 8
            $('.feedback_evaluation_determinants').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_determinants').val()) + 1); i++) {
                    var value = parseFloat($('select#determinants_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_determinants').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseStructurePhrasesListener = function(){// structure de Phrases 9
            $('.feedback_evaluation_structurePhrases').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_structurePhrases').val()) + 1); i++) {
                    var value = parseFloat($('select#structurePhrases_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_structurePhrases').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseCalendrierListener = function(){// Calendrier 10
            $('.feedback_evaluation_calendrier').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_calendrier').val()) + 1); i++) {
                    var value = parseFloat($('select#calendrier_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_calendrier').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseCultureListener = function(){// Culture et Civilisation 11
            $('.feedback_evaluation_cultures').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_cultures').val()) + 1); i++) {
                    var value = parseFloat($('select#cultures_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_cultures').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseRepetitionOraleListener = function(){// repetitionOrale 12
            $('.feedback_evaluation_repetitionOrale').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_repetitionOrale').val()) + 1); i++) {
                    var value = parseFloat($('select#repetitionOrale_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_repetitionOrale').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseDicteeListener = function(){// Dictee 13
            $('.feedback_evaluation_dictee').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_dictee').val()) + 1); i++) {
                    var value = parseFloat($('select#dictee_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_dictee').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseTraductionTexteListener = function(){// traductionTexte 14
            $('.feedback_evaluation_traductionTexte').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_traductionTexte').val()) + 1); i++) {
                    var value = parseFloat($('select#traductionTexte_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_traductionTexte').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseRedactionGrammaireListener = function(){// redactionGrammaire 15
            $('.feedback_evaluation_redactionGrammaire').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_redactionGrammaire').val()) + 1); i++) {
                    var value = parseFloat($('select#redactionGrammaire_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_redactionGrammaire').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseRedactionLibreListener = function(){// redactionLibre 16
            $('.feedback_evaluation_redactionLibre').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_redactionLibre').val()) + 1); i++) {
                    var value = parseFloat($('select#redactionLibre_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_redactionLibre').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseComprehensionOralListener = function(){// comprehensionOral 17
            $('.feedback_evaluation_comprehensionOral').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_comprehensionOral').val()) + 1); i++) {
                    var value = parseFloat($('select#comprehensionOral_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_comprehensionOral').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseComprehensionEcritListener = function(){// comprehensionEcrit 18
            $('.feedback_evaluation_comprehensionEcrit').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_comprehensionEcrit').val()) + 1); i++) {
                    var value = parseFloat($('select#comprehensionEcrit_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_comprehensionEcrit').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseVocabulairesListener = function(){// vocabulaires 19
            $('.feedback_evaluation_vocabulaires').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_vocabulaires').val()) + 1); i++) {
                    var value = parseFloat($('select#vocabulaires_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_vocabulaires').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };
        FeedbackEvaluation.prototype.chooseLectureListener = function(){// Lecture 20
            $('.feedback_evaluation_lecture').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_lecture').val()) + 1); i++) {
                    var value = parseFloat($('select#lecture_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_lecture').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };

        FeedbackEvaluation.prototype.chooseExpressionLibreListener = function(){// Expression Libre 21
            $('.feedback_evaluation_expressionLibre').on('change', function () {
                var total = 0;
                for (var i = 1; i < (parseInt($('input#hidden_expressionLibre').val()) + 1); i++) {
                    var value = parseFloat($('select#expressionLibre_select_'+i).val());
                    if (isNaN(value)){
                        total += 0;
                    }else {
                        total += value;
                    }
                }
                $('input#input_expressionLibre').val(total);
                feedback_evaluation.calculateTotalMarks();
            });
        };


        FeedbackEvaluation.prototype.calculateTotalMarks = function () {
            var totalQuestion = $('input#total_questions').val();
            var totalFinal = 0;
            totalFinal += isNaN(parseFloat($('input#input_noms').val())) ? 0 : parseFloat($('input#input_noms').val());
            totalFinal += isNaN(parseFloat($('input#input_pronoms').val())) ? 0 : parseFloat($('input#input_pronoms').val());
            totalFinal += isNaN(parseFloat($('input#input_verbes').val())) ? 0 : parseFloat($('input#input_verbes').val());
            totalFinal += isNaN(parseFloat($('input#input_adjectifs').val())) ? 0 : parseFloat($('input#input_adjectifs').val());
            totalFinal += isNaN(parseFloat($('input#input_adverbes').val())) ? 0 : parseFloat($('input#input_adverbes').val());
            totalFinal += isNaN(parseFloat($('input#input_prepositions').val())) ? 0 : parseFloat($('input#input_prepositions').val());
            totalFinal += isNaN(parseFloat($('input#input_conjonctions').val())) ? 0 : parseFloat($('input#input_conjonctions').val());
            totalFinal += isNaN(parseFloat($('input#input_determinants').val())) ? 0 : parseFloat($('input#input_determinants').val());
            totalFinal += isNaN(parseFloat($('input#input_structurePhrases').val())) ? 0 : parseFloat($('input#input_structurePhrases').val());
            totalFinal += isNaN(parseFloat($('input#input_calendrier').val())) ? 0 : parseFloat($('input#input_calendrier').val());
            totalFinal += isNaN(parseFloat($('input#input_cultures').val())) ? 0 : parseFloat($('input#input_cultures').val());
            totalFinal += isNaN(parseFloat($('input#input_repetitionOrale').val())) ? 0 : parseFloat($('input#input_repetitionOrale').val());
            totalFinal += isNaN(parseFloat($('input#input_dictee').val())) ? 0 : parseFloat($('input#input_dictee').val());
            totalFinal += isNaN(parseFloat($('input#input_traductionTexte').val())) ? 0 : parseFloat($('input#input_traductionTexte').val());
            totalFinal += isNaN(parseFloat($('input#input_redactionGrammaire').val())) ? 0 : parseFloat($('input#input_redactionGrammaire').val());
            totalFinal += isNaN(parseFloat($('input#input_redactionLibre').val())) ? 0 : parseFloat($('input#input_redactionLibre').val());
            totalFinal += isNaN(parseFloat($('input#input_comprehensionOral').val())) ? 0 : parseFloat($('input#input_comprehensionOral').val());
            totalFinal += isNaN(parseFloat($('input#input_comprehensionEcrit').val())) ? 0 : parseFloat($('input#input_comprehensionEcrit').val());
            totalFinal += isNaN(parseFloat($('input#input_vocabulaires').val())) ? 0 : parseFloat($('input#input_vocabulaires').val());
            totalFinal += isNaN(parseFloat($('input#input_lecture').val())) ? 0 : parseFloat($('input#input_lecture').val());
            totalFinal += isNaN(parseFloat($('input#input_expressionLibre').val())) ? 0 : parseFloat($('input#input_expressionLibre').val());
            var noteFinale = (totalFinal * 70)/totalQuestion;
            $('input#note_finale_evaluation').val(noteFinale.toFixed(2));
            $('input#hidden_note_finale_evaluation').val(noteFinale.toFixed(2));
        };
        //this should be at the end
        feedback_evaluation.initializeView();
        feedback_evaluation.setListeners();
    });
});