
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function ServiceAcademique() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var devoir = new ServiceAcademique();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        ServiceAcademique.prototype.initializeView = function() {};

        /**
         * allow to set a whole bunch of listeners
         */
        ServiceAcademique.prototype.setListeners = function() {
            this.submitAttachedTeacherListener();
            this.submitAttachedSupervisorsListener();
        };

        ServiceAcademique.prototype.submitAttachedTeacherListener = function(){
            $('input[name="save"]').on('click', function (e) {
                e.preventDefault();

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir affecter ces apprenants à ces enseignants ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $('#teachers').submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        ServiceAcademique.prototype.submitAttachedSupervisorsListener = function(){
            $('input[name="set_supervisors"]').on('click', function (e) {
                e.preventDefault();

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir affecter ces enseignants à ces superviseurs ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $('#superviseurs').submit();
                };
                devoir.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        //this should be at the end
        devoir.initializeView();
        devoir.setListeners();
    });
});