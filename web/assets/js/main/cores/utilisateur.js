
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Utilisateur() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var utilisateur = new Utilisateur();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Utilisateur.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Utilisateur.prototype.setListeners = function() {
            this.validateUtilisateurListener();
            this.validateFreeUtilisateurListener();
            this.changeStatutUtilisateurListener();
            this.freeUtilisateurListener();
            this.reinitializeTrainingListener();

        };

        Utilisateur.prototype.validateUtilisateurListener = function(){
            $('.ajax-utilisateur-history-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'un utilisateur',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer tous les informations liées à cet utilisateur sur la plateforme\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': 'Les informations de cet utilisateurs ont été supprimé avec succès',
                                    'type': 'success'
                                }));
                                window.location.href = APP_ROOT + '/admin';
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                utilisateur.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };

        Utilisateur.prototype.validateFreeUtilisateurListener = function(){
            $('.ajax-free-utilisateur-history-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'un utilisateur',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer tous les informations liées à cet utilisateur sur la plateforme\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': 'Les informations de cet utilisateurs ont été supprimé avec succès',
                                    'type': 'success'
                                }));
                                window.location.href = APP_ROOT + '/admin';
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                utilisateur.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };

        Utilisateur.prototype.changeStatutUtilisateurListener = function(){
            $('.ajax-utilisateur-change-status').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Changement de statut",
                    'message' : 'En confirmant, vous certifiez que vous voulez changer le statut de cet apprenant en mode gratuit.',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': "Le statut de l'apprenant a été modifié avec succès.",
                                    'type': 'success'
                                }));
                                location.reload();
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Changement de statut impossible',
                                    'message': 'Le changement de statut ne peut pas être modifié.',
                                    'type': 'error'
                                }));
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                utilisateur.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Utilisateur.prototype.freeUtilisateurListener = function(){
            $('.ajax-utilisateur-regular-status').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Changement de statut",
                    'message' : 'En confirmant, vous certifiez que vous voulez changer le statut de cet apprenant en mode regulier.',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': "Le statut de l'apprenant a été modifié avec succès.",
                                    'type': 'success'
                                }));
                                location.reload();
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Changement de statut impossible',
                                    'message': 'Le changement de statut ne peut pas être modifié.',
                                    'type': 'error'
                                }));
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                utilisateur.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Utilisateur.prototype.reinitializeTrainingListener = function(){
            $('.ajax-user-reinitialize-training').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Réinitialisation de la formation",
                    'message' : 'En confirmant, vous certifiez que vous voulez réinitialiser la formation de cet apprenant',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "anglaisenligne.cm",
                                    'message': "La formation a été réinitialisé avec succès.",
                                    'type': 'success'
                                }));
                                location.reload();
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'anglaisenligne.cm',
                                    'message': 'Impossible',
                                    'type': 'error'
                                }));
                                utilisateur.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                utilisateur.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        //this should be at the end
        utilisateur.initializeView();
        utilisateur.setListeners();
    });
});