
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function PaymentMethod() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var payment_method = new PaymentMethod();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        PaymentMethod.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        PaymentMethod.prototype.setListeners = function() {
            this.validatePaymentMethodListener();
            this.payPalMethodListener();
        };

        PaymentMethod.prototype.validatePaymentMethodListener = function(){
            $('.ajax-payment_method-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'une méthode de paiement',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Suppression d'un moyen de paiement",
                                    'message': 'Le moyen de paiement a été supprimé',
                                    'type': 'success'
                                }));
                                location.reload();
                                payment_method.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                payment_method.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                payment_method.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };
        PaymentMethod.prototype.payPalMethodListener = function(){
            $('.ajax-paypal-link').on('click', function (e) {
                e.preventDefault();
                var feedbackCourse = JSON.parse(JSON.stringify({
                    'title': "anglaisenligne.cm",
                    'message': 'Cette fonctionnalité est en cours de développement. Elle serait disponible dans les prochains jours.',
                    'type': 'info'
                }));
                payment_method.feedbackHelper.showAutoCloseMessage(feedbackCourse.title, feedbackCourse.message, feedbackCourse.type);
            });
        };
        //this should be at the end
        payment_method.initializeView();
        payment_method.setListeners();

    });
});