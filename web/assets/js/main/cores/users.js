
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Users() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var users = new Users();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Users.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Users.prototype.setListeners = function() {
            this.enableUsersListener();
            this.disableUsersListener();
        };

        Users.prototype.enableUsersListener = function(){
            $('.ajax-user-disable').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Désactiver le compte d'un utilisateur.",
                    'message' : "En confirmant, vous certifiez que vous voulez désactiver le compte de cet utilisateur, voulez vous continuer ?\n NB: Cet utilisateur ne pourra plus se connecter à la plateforme.",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Désactiver un compte",
                                    'message': "L'utilisateur a été désactivé.",
                                    'type': 'success'
                                }));
                                location.reload();
                                users.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Désactivation impossible',
                                    'message': 'La désactivation du compte a echoué',
                                    'type': 'error'
                                }));
                                users.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                users.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Users.prototype.disableUsersListener = function(){
            $('.ajax-user-enable').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Activer le compte d'un utilisateur.",
                    'message' : "En confirmant, vous certifiez que vous voulez activer le compte de cet utilisateur, voulez vous continuer ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Activer un compte",
                                    'message': "Le compte utilisateur a été activé.",
                                    'type': 'success'
                                }));
                                location.reload();
                                users.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Activation impossible',
                                    'message': "L'activation du compte a échoué",
                                    'type': 'error'
                                }));
                                users.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                users.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };
        //this should be at the end
        users.initializeView();
        users.setListeners();
    });
});