
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Coaching() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var coaching = new Coaching();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Coaching.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Coaching.prototype.setListeners = function() {
            this.showModalListener();
            this.reprogramCoaching();
        };

        Coaching.prototype.showModalListener = function() {
            $('a[name="add-coaching"]').on('click', function (e) {
                e.preventDefault();
                var that = this;
                $('#day').text($(this).data('date'));
                $('#hour').text($(this).data('hour'));

                var url = $(this).data('url');

                $(that).parent().parent().parent().LoadingOverlay('show');
                $('#coaching-form').load(url, function () {
                    $(that).parent().parent().parent().LoadingOverlay('hide');
                });
            })
        };

        Coaching.prototype.reprogramCoaching = function() {
            $('a[name="reprogram-coaching"]').on('click', function (e) {
                e.preventDefault();
                var link = $(this).data('url');
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Reprogrammation d'un coaching",
                    'message' : 'En confirmant, vous certifiez que vous voulez reprogrammer votre session de coaching oral, voulez vous continuer  ?\n\n NB: Vous ne pouvez effectuer cette opération qu\'au maximum 3 fois',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));

                var callback = function () {
                    window.location.href = link;
                };
                coaching.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            })
        };

        //this should be at the end
        coaching.initializeView();
        coaching.setListeners();
    });
});