
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Palier() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var palier = new Palier();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Palier.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Palier.prototype.setListeners = function() {
            this.choosePalierListener();
            this.chooseNiveauListener();
        };

        Palier.prototype.choosePalierListener = function(){
            $('.ajax-palier-choose').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Etes-vous sûr de vouloir souscrire au " + link.data('value')+"?",
                    'message' : "",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Succès",
                                    'message': "Le palier a été choisi avec succès. Vous pouvez procéder à la validation du paiement de votre formation.",
                                    'type': 'success'
                                }));
                                palier.feedbackHelper.showMessageWithPromptCallBack(feedback.title, feedback.message, feedback.type, function () {
                                    window.location.href = APP_ROOT + '/student/'+response.id+'/payment/notification';
                                })
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Erreur',
                                    'message': "Le choix du palier a echouée",
                                    'type': 'error'
                                }));
                                palier.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                palier.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        Palier.prototype.chooseNiveauListener = function(){
            $('.ajax-niveau-choose').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Etes-vous sûr de vouloir souscrire au niveau " + link.data('value')+"?",
                    'message' : "",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Choix du niveau",
                                    'message': 'Le niveau a été choisi avec succès, Voulez-vous proceder au paiement maintenant?',
                                    'type': 'success',
                                    'confirmeButtonText' : 'Je le confirme'
                                }));
                                var callback1 = function () {
                                    window.location.href = APP_ROOT + '/student/'+response.id+'/payment/notification';
                                };
                                palier.feedbackHelper.showLoaderMessage(feedback.title, feedback.message, feedback.type, feedbackMessage.confirmeButtonText, callback1)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Choix Impossible',
                                    'message': "Le choix du niveau a echouée",
                                    'type': 'error'
                                }));
                                palier.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                palier.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        //this should be at the end
        palier.initializeView();
        palier.setListeners();
    });
});