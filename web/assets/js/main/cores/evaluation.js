
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Evaluation() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var evaluation = new Evaluation();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Evaluation.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Evaluation.prototype.setListeners = function() {
            this.evaluationSubmitListener();
            this.evaluationAutoSubmitListener();
            this.chooseSelectListener();
            this.inputListener();
            this.textAreaListener();
        };

        Evaluation.prototype.evaluationSubmitListener = function(){
            $('.ajax-evaluation-submit').on('click', function (e) {
                e.preventDefault();
                var orale = parseInt($('input#evaluation_orale').val());// Recupère la formation en cours
                if(orale !== 0){
                    var counter = parseInt($('input#counter_audio').val());// Recupère le nombre d'audio de l'évaluation
                    var audioCounter = 0;
                    for (var i = 1; i <= counter; i++) {
                        var audio = $('#audio_path_' + i).val();
                        if (audio !== ''){
                            audioCounter += 1;
                        }
                    }

                    if (audioCounter !== counter){
                        var remainCounter = counter - audioCounter;
                        var feedbackWarning = JSON.parse(JSON.stringify({
                            'title' : "anglaisenligne.cm",
                            'message' : "Bien vouloir enregistrer tous les audios avant de soumettre votre évaluation. \n Il vous reste "+ remainCounter + " audios non enregistrés",
                            'type' : 'warning'
                        }));
                        evaluation.feedbackHelper.showAutoCloseMessage(feedbackWarning.title, feedbackWarning.message, feedbackWarning.type)
                        return;
                    }
                }

                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "anglaisenligne.cm",
                    'message' : "Etes-vous sûr de vouloir soumettre cette partie de l'évaluation ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var form = $('#form_evaluation');
                    var training = $('input#training').val();// Recupère la formation en cours
                    var niveau = $('input#niveau').val();// Recupère le niveau de formation
                    var url = $('input#url').val();// Recupère l'url du formulaire a soumettre
                    var partie = $('input#evaluation_part').val();// Recupère l'audio s'il existe

                    var orale = parseInt($('input#evaluation_orale').val());// Recupère la formation en cours
                    if(orale !== 0) {
                        var audio1 = $('input#audio_path_1').val();// Recupère le premier audio de l'expression orale
                        var audio2 = $('input#audio_path_2').val();// Recupère le deuxième exercice de l'expression orale
                        var audio3 = $('input#audio_path_3').val();// Recupère l'audio de l'expression libre
                        var audio4 = $('input#audio_path_4').val();// Recupère l'audio de l'expression libre
                        var audio5 = $('input#audio_path_5').val();// Recupère l'audio de l'expression libre
                        var audio6 = $('input#audio_path_6').val();// Recupère l'audio de l'expression libre
                        var audio7 = $('input#audio_path_7').val();// Recupère l'audio de l'expression libre
                        var audio8 = $('input#audio_path_8').val();// Recupère l'audio de l'expression libre

                        audio1 = audio1 === undefined ? '' : audio1;
                        audio2 = audio2 === undefined ? '' : audio2;
                        audio3 = audio3 === undefined ? '' : audio3;
                        audio4 = audio4 === undefined ? '' : audio4;
                        audio5 = audio5 === undefined ? '' : audio5;
                        audio6 = audio6 === undefined ? '' : audio6;
                        audio7 = audio7 === undefined ? '' : audio7;
                        audio8 = audio8 === undefined ? '' : audio8;

                        var data = JSON.parse(JSON.stringify({
                            'training': training,
                            'niveau': niveau,
                            'partie': partie,
                            'audio_1': audio1,
                            'audio_2': audio2,
                            'audio_3': audio3,
                            'audio_4': audio4,
                            'audio_5': audio5,
                            'audio_6': audio6,
                            'audio_7': audio7,
                            'audio_8': audio8,
                            'form': form.html()
                        }));
                    }else {
                        var data = JSON.parse(JSON.stringify({
                            'training': training,
                            'niveau': niveau,
                            'partie': partie,
                            'form': form.html()
                        }));
                    }
                    //2. Post avec AJAX
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {data: data},
                        success: function (response) {
                            console.log(response);
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Evaluation",
                                    'message': "Merci d’avoir soumis une partie de l'évaluation du niveau "+ response.niveau +". Le service académique de anglaisenligne.cm se chargera du retour vos notes dans un délai de 48 heures.",
                                    'type': 'success'
                                }));
                                evaluation.feedbackHelper.showMessageWithPromptCallBack(feedback.title, feedback.message, feedback.type, function () {
                                    window.location.href = APP_ROOT + '/choose/student/' + response.id_training + '/start/evaluation/course/niveau';
                                })
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': "Erreur",
                                    'message': "Impossible de soumettre cette partie de  l'évaluation, Si le problème persiste, Bien vouloir contacter le service client de anglaisenligne.cm",
                                    'type': 'error'
                                }));
                                evaluation.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                evaluation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Evaluation.prototype.evaluationAutoSubmitListener = function(){
            $('.ajax-auto-evaluation-submit').on('click', function (e) {
                e.preventDefault();
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Soumettre une partie de l'évaluation",
                    'message' : "Etes-vous sûr de vouloir soumettre cette partie de l'évaluation ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    var form = $('#form_evaluation');
                    var training = $('input#training').val();// Recupère la formation en cours
                    var niveau = $('input#niveau').val();// Recupère le niveau de formation
                    var partie = $('input#evaluation_part').val();// Recupère l'audio s'il existe
                    var url = $('input#url').val();// Recupère l'url du formulaire a soumettre

                    var orale = parseInt($('input#evaluation_orale').val());// Recupère la formation en cours
                    if(orale !== 0) {
                        var audio1 = $('input#audio_path_1').val();// Recupère le premier audio de l'expression orale
                        var audio2 = $('input#audio_path_2').val();// Recupère le deuxième exercice de l'expression orale
                        var audio3 = $('input#audio_path_3').val();// Recupère l'audio de l'expression libre
                        var audio4 = $('input#audio_path_4').val();// Recupère l'audio de l'expression libre
                        var audio5 = $('input#audio_path_5').val();// Recupère l'audio de l'expression libre
                        var audio6 = $('input#audio_path_6').val();// Recupère l'audio de l'expression libre
                        var audio7 = $('input#audio_path_7').val();// Recupère l'audio de l'expression libre
                        var audio8 = $('input#audio_path_8').val();// Recupère l'audio de l'expression libre

                        audio1 = audio1 === undefined ? '' : audio1;
                        audio2 = audio2 === undefined ? '' : audio2;
                        audio3 = audio3 === undefined ? '' : audio3;
                        audio4 = audio4 === undefined ? '' : audio4;
                        audio5 = audio5 === undefined ? '' : audio5;
                        audio6 = audio6 === undefined ? '' : audio6;
                        audio7 = audio7 === undefined ? '' : audio7;
                        audio8 = audio8 === undefined ? '' : audio8;

                        var data = JSON.parse(JSON.stringify({
                            'training': training,
                            'niveau': niveau,
                            'partie': partie,
                            'audio_1': audio1,
                            'audio_2': audio2,
                            'audio_3': audio3,
                            'audio_4': audio4,
                            'audio_5': audio5,
                            'audio_6': audio6,
                            'audio_7': audio7,
                            'audio_8': audio8,
                            'form': form.html()
                        }));
                    }else{
                        var data = JSON.parse(JSON.stringify({
                            'training': training,
                            'niveau': niveau,
                            'partie': partie,
                            'form': form.html()
                        }));
                    }
                    //2. Post avec AJAX
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {data: data},
                        success: function (response) {
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Succès",
                                    'message': "Merci d’avoir passer une partie de l'évaluation du niveau "+ response.niveau +". Le service académique de anglaisenligne.cm se chargera du retour vos notes dans un délai de 48 heures.",
                                    'type': 'success'
                                }));
                                evaluation.feedbackHelper.showMessageWithPromptCallBack(feedback.title, feedback.message, feedback.type, function () {
                                    window.location.href = APP_ROOT + '/student/active';
                                })
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': "Erreur",
                                    'message': "Impossible de soumettre cette partie de l'évaluation, Si le problème persiste, Bien vouloir contacter le service client de anglaisenligne.cm",
                                    'type': 'error'
                                }));
                                evaluation.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                evaluation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });
        };

        Evaluation.prototype.chooseSelectListener = function(){
            $('.save_eval').on('change', function () {
                var that= this;
                var value = $(that).val();
                $(this).find('option').each(function () {
                    if ($(this).text() == value){
                        $(this).attr("selected", "selected")
                    }else {
                        $(this).removeAttr("selected")
                    }
                });
            })
        };

        Evaluation.prototype.inputListener = function(){
            $('input.form-control').on('change', function () {
                var value = $(this).val();
                $(this).attr("value", value);
            })
        };

        Evaluation.prototype.textAreaListener = function(){
            $('textarea.form-control').on('change', function () {
                var value = $(this).val();
                $(this).attr("value", value);
                $(this).text(value);
            })
        };

        //this should be at the end
        evaluation.initializeView();
        evaluation.setListeners();
    });
});