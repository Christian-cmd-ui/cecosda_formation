
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 22/12/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Evaluation() {
            this.feedbackHelper = new FeedbackHelper();
            this.imageUrl = null
        }

        var evaluation = new Evaluation();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Evaluation.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Evaluation.prototype.setListeners = function() {
            this.chooseEvaluationListener();
        };

        Evaluation.prototype.chooseEvaluationListener = function(){
            $('.ajax-evaluation-submit').on('click', function (e) {
                e.preventDefault();
                var div = document.getElementById("photo");
                document.getElementById('submit_devoir').style.display="none";
                document.getElementById('myHeader').style.display="none";
                document.getElementById('cancel_button').style.display="none";
                // Use the html2canvas
                // function to take a screenshot
                // and append it
                // to the output div

                    html2canvas(div).then(
                        function (canvas) {
                            document.getElementById('output').appendChild(canvas); // prévisualisation
                            //Cachez la prévisualisation
                            document.getElementsByTagName('canvas')[0].style.display="none";
                            //Envoie du fichier au serveur
                            //1. Récupération URL
                            canvas = document.getElementsByTagName('canvas')[0];
                            //console.log(canvas.toDataURL());
                            evaluation.imageUrl = canvas.toDataURL();

                        });


                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : "Soumettre l'évaluation",
                    'message' : "Etes-vous sûr de vouloir soumettre l'évaluation ?",
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                setTimeout((function() {
                var callback = function () {
                    var training = $('input#training').val();// Recupere la formation en cours
                    var niveau = $('input#niveau').val();// Recupere le niveau de formation
                    var url = $('input#url').val();// Recupere l'url du formulaire a soummettre

                    var data  = {image: evaluation.imageUrl, training: training, niveau: niveau};
                    //2. Post avec AJAX
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            console.log(response);
                            if (response.status === true) {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Evaluation",
                                    'message': "L'évaluation a été soumise avec succès, Bien vouloir consulter votre espace d'évaluation ",
                                    'type': 'success'
                                }));
                                evaluation.feedbackHelper.showMessageWithPromptCallBack(feedback.title, feedback.message, feedback.type, function () {
                                    window.location.href = APP_ROOT + '/student/active';
                                })
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': "Impossible de soumettre l'évaluation",
                                    'message': "Impossible de soumettre l'évaluation, Si le problème persiste, Bien vouloir contacter l'administrateur",
                                    'type': 'error'
                                }));
                                evaluation.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                evaluation.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
                }), 3000);
            });

        };
        //this should be at the end
        evaluation.initializeView();
        evaluation.setListeners();
    });
});