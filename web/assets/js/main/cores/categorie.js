
//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    head.load([
        APP_ROOT + "/assets/js/main/helpers/FeedbackHelper.js"
    ], function() {
        /**
         * constructor
         */
        function Categorie() {
            this.feedbackHelper = new FeedbackHelper();
        }

        var categorie = new Categorie();

        /**
         * allow to initialize the view
         * @return {void} nothing
         */
        Categorie.prototype.initializeView = function() {
        };

        /**
         * allow to set a whole bunch of listeners
         */
        Categorie.prototype.setListeners = function() {
            this.validateCategorieListener();
        };

        Categorie.prototype.validateCategorieListener = function(){
            $('.ajax-categorie-delete').on('click', function (e) {
                e.preventDefault();
                var link = $(this);
                var feedbackMessage = JSON.parse(JSON.stringify({
                    'title' : 'Suppression d\'une catégorie',
                    'message' : 'En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible',
                    'type' : 'warning',
                    'confirmeButtonText' : 'Je le confirme'
                }));
                var callback = function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'DELETE',
                        url: link.data('url'),
                        success: function (response) {
                            if (response.status === 'success') {
                                var feedback = JSON.parse(JSON.stringify({
                                    'title': "Suppression d'une catégorie",
                                    'message': 'La catégorie a été supprimé',
                                    'type': 'success'
                                }));
                                location.reload();
                                categorie.feedbackHelper.showAutoCloseMessage(feedback.title, feedback.message, feedback.type)
                            } else {
                                var feedbackError = JSON.parse(JSON.stringify({
                                    'title': 'Suppression Impossible',
                                    'message': 'La suppression a echoué',
                                    'type': 'error'
                                }));
                                categorie.feedbackHelper.showAutoCloseMessage(feedbackError.title, response.message, feedbackError.type)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown)
                        }
                    });
                };
                categorie.feedbackHelper.showLoaderMessage(feedbackMessage.title, feedbackMessage.message, feedbackMessage.type, feedbackMessage.confirmeButtonText, callback);
            });

        };
        //this should be at the end
        categorie.initializeView();
        categorie.setListeners();

    });
});