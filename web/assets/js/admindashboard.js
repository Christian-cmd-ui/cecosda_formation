(function (e, ch, fn, cx, C) {
  var as = window.as || {},
    period,
    adminAjaxLink,
    graphWrapper,
    yearLabels,
    weekLabels,
    weekLabelsLinks,
    currentPeriodText;

  as.initFields = function () {
    period = e("#period");
    graphWrapper = e("#graph-wrapper");
    adminAjaxLink = e("#admin_ajax_link").val();
    currentPeriodText = e("#current_period_text");
    yearLabels = e("#year-labels");
    weekLabels = e("#week-labels");
    weekLabelsLinks = weekLabels.children().children();
  };

  as.handlePeriodChange = function () {
    let that = this;
    as.loading();
    $.ajax({
      url: adminAjaxLink,
      type: "POST",
      dataType: "json",
      data: {
        period: $(this).val(),
      },
      async: true,
      success: function (data) {
        if (Array.isArray(data)) {
          ch.destroy();
          currentPeriodText.html($(that).find("option:selected").text());
          document.getElementById("statistique_connexion").remove();
          let canvas = document.createElement("canvas");
          canvas.setAttribute("id", "statistique_connexion");
          canvas.setAttribute("width", "400");
          canvas.setAttribute("height", "150");
          document.querySelector("#chart-container").appendChild(canvas);
          ch = new C(
            document.getElementById("statistique_connexion").getContext("2d"),
            fn(data)
          );
          if (data.length == 12) {
            yearLabels.slideDown();
            weekLabels.slideUp();
          } else {
            weekLabels.slideDown();
            yearLabels.slideUp();
          }
        }
      },
    }).done(function () {
      as.idle();
    });
  };

  as.handleWeekLabelsLinksClicks = function (e) {
    e.preventDefault();
    let link = $(this).prop("href") + "?month=" + period.val();
    window.location.assign(link);
  };

  as.listen = function () {
    console.log(weekLabelsLinks);
    e("#period").on("change", as.handlePeriodChange);
    weekLabelsLinks.on("click", as.handleWeekLabelsLinksClicks);
  };
  as.loading = function () {
    graphWrapper.css({ opacity: 0.3, cursor: "progress" });
  };
  as.idle = function () {
    graphWrapper.css({ opacity: 1, cursor: "inherit" });
  };
  as.init = function () {
    as.initFields();
    as.listen();
  };
  e(document).ready(as.init);
})($, loginChart, getChartOptions, ctx, Chart);
