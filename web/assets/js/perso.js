// Lancer le tooltip de façon permanante
$('.postion').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');
$('.postion2').tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');

var tab1 = 0;
var tab2 = 1;
var tab3 = 1;
var tab4 = 1;
var compteur = 1;
function checkTab(tab) {
	if(tab == '1'){
		compteur += tab1;
		return;
	}
	if(tab == '2'){
		compteur += tab2;
		return;
	}
	if(tab == '3'){
		compteur += tab3;
		return;
	}
	if(tab == '4'){
		compteur += tab4;
		return;
	}

	//On vérifie s'il a cliqué partout
	if (compteur < 4) {
		alert("Vous devez parcourrir tous les éléments du cours avant d'avancer");
	}
}

var id = null;
function myMove() {
  var elem = document.getElementById("myAnimation");
  if (elem){
      var pos = 0;
      clearInterval(id);
      id = setInterval(frame, 10);
      function frame() {
          if (pos == 300) {
              elem.style.left = 0 + 'px';
              clearInterval(id);
          } else {
              pos++;
              elem.style.left = pos + 'px';
          }
      }
  }
}

function myFunction() {
  setInterval(function(){
	var id = null;
  	myMove();
  }, 8000);
}
myMove();
myFunction();
