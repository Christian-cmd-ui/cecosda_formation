//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 18/09/2020

$(function () {
  var APP_ROOT;

  window.location.pathname.match(/(.*?)web/i)
    ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1])
    : (APP_ROOT = "");
  APP_ROOT ? (APP_ROOT += "web") : APP_ROOT;

  head.load([APP_ROOT + "/assets/js/FeedbackHelper.js"], function () {
    /**
     * constructor
     */
    function Partie() {
      this.feedbackHelper = new FeedbackHelper();
      this.counter = 0;
    }

    var partie = new Partie();

    /**
     * allow to initialize the view
     * @return {void} nothing
     */
    Partie.prototype.initializeView = function () {
      var notification = document.querySelector(".hidden_message");
      if (notification && partie.counter === 0) {
        partie.counter++;
        var message = $("input#hidden_flash_bag_message").val();
        var type = $("input#hidden_flash_bag_type").val();
        var feedback = JSON.parse(
          JSON.stringify({
            title: "cecosdaformation.com",
            message: message,
            type: type,
          })
        );
        partie.feedbackHelper.showAutoCloseMessage(
          feedback.title,
          feedback.message,
          feedback.type
        );
      }

      var notif = document.querySelector(".hidden_notification");
      if (notif) {
        var number = $("input#hidden_notification").val();
        var feedbackCourse = JSON.parse(
          JSON.stringify({
            title: "cecosdaformation.com",
            message:
              "Il vous reste " +
              number +
              " sous rubriques à parcourir avant le devoir obligatoire.",
            type: "info",
          })
        );
        partie.feedbackHelper.showAutoCloseMessage(
          feedbackCourse.title,
          feedbackCourse.message,
          feedbackCourse.type
        );
      }

      var url = $("input#useful_link").val(); // Recupère l'url dans base_student.html.twig

      if (url !== undefined) {
        var badge = document.querySelector(".load_badges");
        if (badge) {
          $.ajax({
            method: "POST",
            url: url,
            success: function (response) {
              if (response.status === true) {
                if (response.reservation_start > 0) {
                  var badge1 = document.getElementById("reservations_start");
                  if (badge1) {
                    badge1.style.display = "block";
                    badge1.innerHTML = response.reservation_start;
                  }
                }
                if (response.reservation_paid > 0) {
                  var badge2 = document.getElementById("reservations_paid");
                  if (badge2) {
                    badge2.style.display = "block";
                    badge2.innerHTML = response.reservation_paid;
                  }
                }
                if (response.reservation_confirmed > 0) {
                  var badge3 = document.getElementById(
                    "reservations_confirmed"
                  );
                  if (badge3) {
                    badge3.style.display = "block";
                    badge3.innerHTML = response.reservation_confirmed;
                  }
                }
                if (response.devoir_submitted > 0) {
                  var badge4 = document.getElementById("devoirs_submitted");
                  if (badge4) {
                    badge4.innerHTML = response.devoir_submitted;
                    badge4.style.display = "block";
                  }
                }
                if (response.devoir_corrected > 0) {
                  var badge5 = document.getElementById("devoirs_corrected");
                  if (badge5) {
                    badge5.style.display = "block";
                    badge5.innerHTML = response.devoir_corrected;
                  }
                }
                if (response.evaluation_submitted > 0) {
                  var badge6 = document.getElementById("evaluations_submitted");
                  if (badge6) {
                    badge6.style.display = "block";
                    badge6.innerHTML = response.evaluation_submitted;
                  }
                }

                if (response.evaluation_corrected > 0) {
                  var badge7 = document.getElementById("evaluations_corrected");
                  if (badge7) {
                    badge7.style.display = "block";
                    badge7.innerHTML = response.evaluation_corrected;
                  }
                }

                if (response.coaching_start > 0) {
                  var badge8 = document.getElementById("coachings_start");
                  if (badge8) {
                    badge8.style.display = "block";
                    badge8.innerHTML = response.coaching_start;
                  }
                }
                if (response.coaching_paid > 0) {
                  var badge9 = document.getElementById("coachings_paid");
                  if (badge9) {
                    document.getElementById("coachings_paid").style.display =
                      "block";
                    document.getElementById("coachings_paid").innerHTML =
                      response.coaching_paid;
                  }
                }
                if (response.coaching_confirmed > 0) {
                  var badge10 = document.getElementById("coachings_confirmed");
                  if (badge10) {
                    badge10.style.display = "block";
                    badge10.innerHTML = response.coaching_confirmed;
                  }
                }
                if (response.attestations_generated > 0) {
                  console.log(response.attestations_generated);
                  var badge11 = document.getElementById(
                    "attestations_generated"
                  );
                  if (badge11) {
                    badge11.style.display = "block";
                    badge11.innerHTML = response.attestations_generated;
                  }
                }
              } else {
                console.log("Erreur de recuperation des données");
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.log(jqXHR, textStatus, errorThrown);
            },
          });
        }
      }

      var dashboard = document.querySelector("#chart-dashboard");
      if (dashboard) {
        var urlStat = $("input#statistic_link").val();
        if (urlStat !== undefined) {
          $.ajax({
            method: "GET",
            url: urlStat,
            dataType: "JSON",
            success: function (response) {
              var labelsMonth = [];
              var data = [];
              var data_general = [];

              //$.each(response.months, function (index, value) {
              $.each(
                ["Janvier", "Février", "Mars", "Avril"],
                function (index, value) {
                  labelsMonth.push(value);
                }
              );

              //$.each(response.amounts, function (index, value) {
              $.each([12, 23, 43, 2], function (index, value) {
                data.push(value);
              });

              //$.each(response.amounts_general, function (index, value) {
              $.each([76, 98, 32, 0], function (index, value) {
                data_general.push(value);
              });

              var $gradients3 = [];

              var ctx = document
                .getElementById("admin-dashboard-chart-3")
                .getContext("2d");

              //  for (var i = 0; i < response.types.length; i++){
              for (var i = 0; i < 3; i++) {
                //Get the Hexadecimal color
                var randomColor =
                  "#" +
                  (0x1000000 + Math.random() * 0xffffff)
                    .toString(16)
                    .substr(1, 6);
                // get the rgba color randomly
                var o = Math.round,
                  r = Math.random,
                  s = 255;
                var rgbaColor =
                  "rgba(" +
                  o(r() * s) +
                  "," +
                  o(r() * s) +
                  "," +
                  o(r() * s) +
                  "," +
                  r().toFixed(1) +
                  ")";

                var gradientStroke = ctx.createLinearGradient(0, 0, 0, 300);
                gradientStroke.addColorStop(0, randomColor);
                gradientStroke.addColorStop(1, rgbaColor);
                $gradients3.push(gradientStroke);
              }

              var dataYear = [];
              //$.each(response.types, function (index, value) {
              $.each(
                ["Devoirs", "Evaluations", "Apprenants"],
                function (index, value) {
                  // console.log(response.amounts_annual[value.name]);
                  var data = {
                    label: value,
                    data: 12434 * (index + 1),
                    pointBorderWidth: 4,
                    pointHoverBackgroundColor: $gradients3[index],
                    backgroundColor: $gradients3[index],
                    borderColor: $gradients3[index],
                    borderWidth: 2,
                  };
                  dataYear.push(data);
                }
              );
              console.log(dataYear);
              var myChart = new Chart(ctx, {
                type: "line",
                data: {
                  labels: labelsMonth,
                  datasets: dataYear,
                },
                options: {
                  legend: {
                    position: "top",
                    display: true,
                    labels: {
                      fontColor: "#eee",
                      boxWidth: 40,
                    },
                  },
                  tooltips: {
                    displayColors: false,
                  },
                  scales: {
                    xAxes: [
                      {
                        ticks: {
                          beginAtZero: true,
                          fontColor: "#eee",
                        },
                        gridLines: {
                          display: true,
                          color: "rgba(221, 221, 221, 0.08)",
                        },
                      },
                    ],
                    yAxes: [
                      {
                        ticks: {
                          beginAtZero: true,
                          fontColor: "#eee",
                        },
                        gridLines: {
                          display: true,
                          color: "rgba(221, 221, 221, 0.08)",
                        },
                      },
                    ],
                  },
                },
              });
            },
            error: function (response) {
              console.log(response);
            },
          });
        }
      }
    };
    /**
     * allow to set a whole bunch of listeners
     */
    Partie.prototype.setListeners = function () {
      //  this.validatePartieListener();
    };

    //this should be at the end
    partie.initializeView();
    partie.setListeners();
  });
});
