//@author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
//@creationDate : 17/09/2020

$(function () {
  var APP_ROOT;

  window.location.pathname.match(/(.*?)web/i)
    ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1])
    : (APP_ROOT = "");
  APP_ROOT ? (APP_ROOT += "web") : APP_ROOT;

  head.load([APP_ROOT + "/assets/js/FeedbackHelper.js"], function () {
    /**
     * constructor
     */
    function Destinataire() {
      this.feedbackHelper = new FeedbackHelper();
    }

    var destinataire = new Destinataire();

    /**
     * allow to initialize the view
     * @return {void} nothing
     */
    Destinataire.prototype.initializeView = function () {};

    /**
     * allow to set a whole bunch of listeners
     */
    Destinataire.prototype.setListeners = function () {
      this.validateDestinataireListener();
    };

    Destinataire.prototype.validateDestinataireListener = function () {
      $(".ajax-destinataire-delete").on("click", function (e) {
        e.preventDefault();
        var link = $(this);
        var feedbackMessage = JSON.parse(
          JSON.stringify({
            title: "Suppression d'une remarque",
            message:
              "En confirmant, vous certifiez que vous voulez supprimer cet élément, voulez vous continuer ?\n NB: Cette opération est irréversible",
            type: "warning",
            confirmeButtonText: "Je le confirme",
          })
        );
        var callback = function () {
          $.ajax({
            headers: {
              "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            method: "DELETE",
            url: link.data("url"),
            success: function (response) {
              var res;
              if (typeof response === "object") {
                res = response;
              } else {
                try {
                  res = JSON.parse(response);
                } catch (error) {
                  console.error(error);
                  res = response;
                }
              }
              if (res.status === "success") {
                var feedback = JSON.parse(
                  JSON.stringify({
                    title: "Suppression d'une remarque",
                    message: "La remarque a été supprimé",
                    type: "success",
                  })
                );
                location.reload();
                destinataire.feedbackHelper.showAutoCloseMessage(
                  feedback.title,
                  feedback.message,
                  feedback.type
                );
              } else {
                var feedbackError = JSON.parse(
                  JSON.stringify({
                    title: "Suppression Impossible",
                    message: "La suppression a echoué",
                    type: "error",
                  })
                );
                destinataire.feedbackHelper.showAutoCloseMessage(
                  feedbackError.title,
                  res.message,
                  feedbackError.type
                );
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.log(jqXHR, textStatus, errorThrown);
            },
          });
        };
        destinataire.feedbackHelper.showLoaderMessage(
          feedbackMessage.title,
          feedbackMessage.message,
          feedbackMessage.type,
          feedbackMessage.confirmeButtonText,
          callback
        );
      });
    };
    //this should be at the end
    destinataire.initializeView();
    destinataire.setListeners();
  });
});
