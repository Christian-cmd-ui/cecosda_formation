(function (e) {
  var items = [],
    loading = false,
    search = "";
  (searchResults = e("#search-results")), (loader = e("#loader"));

  async function getResults(s) {
    return new Promise((resolve) => {
      //var path = "/search?q=";
      var path = "/web/search?q=";
      e.getJSON(path + s, function (json) {
        if (Array.isArray(json.data)) {
          resolve(json.data);
        } else {
          resolve([]);
        }
      }).fail(function () {
        resolve([]);
      });
    });
  }

  function showLoading() {
    loading = true;
    searchResults.hide();
    loader.show();
  }
  function hideLoading() {
    loading = false;
    searchResults.show();
    loader.hide();
  }
  async function performSearch(sear) {
    search = sear;
    const s = search;
    items = [];
    if (s.length < 3) {
      return;
    }
    if (loading) {
      return;
    }

    showLoading();
    items = await getResults(search);
    hideLoading();
  }

  async function autocomplete(btn) {
    function processAutocomplete(that) {
      let val = search;
      closeAllLists();
      if (!val) {
        return;
      }

      performSearch(val).then(function () {
        const count = items.length;
        if (count > 0) {
          for (i = 0; i < count; i++) {
            searchResults.append(`
              <div class="col-md-3 mt-3">
                <a href="${items[i].href}" onclick="$('#searchModal').modal('hide')">
                  <div class="card" style="height: 140px; overflow: hidden">
                    <div class="card-body">
                      <h6 class="card-title">${items[i].title}</h6>
                      <p class="card-text">${items[i].description}</p>
                    </div>
                  </div>
                </a>
              </div>
            `);
          }
        } else {
          searchResults.append(`
            <h5 class="text-center d-block w-100 text-muted">Aucun résultat trouvé !</h5>
          `);
        }
      });
    }

    const processChanges = debounce(processAutocomplete);
    btn.addEventListener("click", (ev) => {
      processChanges(ev.target);
    });

    function closeAllLists(elmnt = null) {
      searchResults.empty();
      search = "";
      if (elmnt) e(inp).val("");
    }

    /*document.addEventListener("click", function (e) {
      closeAllLists(e.target);
    });*/
  }
  function debounce(func, timeout = 150) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  e(document).ready(function () {
    autocomplete(document.getElementById("search-btn"));
    inp = document.getElementById("search-val");
    if (inp) {
      inp.addEventListener("input", (ev) => {
        search = ev.target.value;
        if (searchResults.children().length > 0) searchResults.empty();
      });
    }
  });
})($);
