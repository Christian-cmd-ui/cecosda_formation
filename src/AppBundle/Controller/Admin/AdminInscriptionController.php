<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Inscription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Inscription controller.
 *
 * @Route("admin/inscriptions")
 */
class AdminInscriptionController extends Controller {
  /**
   * Lists all Inscription entities.
   *
   * @Route("/", name="admin_inscriptions_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $inscriptions = $em->getRepository('AppBundle:Inscription')->findAll();

    return $this->render('admin/inscription/index.html.twig', array(
      'inscriptions' => $inscriptions,
      'inscription_link' => 'inscription_index',
    ));
  }

  /**
   * Lists all Inscription entities.
   *
   * @Route("/{id}", name="admin_inscriptions_show")
   * @Method("GET")
   */
  public function show(Inscription $inscription) {
    return $this->render('admin/inscription/show.html.twig', array(
      'inscription' => $inscription,
      'inscription_link' => 'inscription_index',
    ));
  }

  /**
   * Deletes a inscription entity.
   *
   * @Route("/{id}/delete", name="admin_inscriptions_delete")
   * @param Request $request
   * @param Inscription $inscription
   *
   * @return Response
   */
  public function delete(Request $request, Inscription $inscription) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($inscription);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Pré-inscription supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
