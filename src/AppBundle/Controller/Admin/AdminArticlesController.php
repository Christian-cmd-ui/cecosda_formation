<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Articles;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contact controller.
 *
 * @Route("admin/articles")
 */
class AdminArticlesController extends Controller {
  /**
   * Lists all Contact entities.
   *
   * @Route("/", name="admin_articles_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $articles = $em->getRepository('AppBundle:Articles')->findBy([], ['id' => "DESC"]);

    return $this->render('admin/blog/index.html.twig', array(
      'articles' => $articles
    ));
  }
 /**
   * Creates a new articles entity.
   *
   * @Route("/new", name="admin_articles_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function create(Request $request) {
    $article = new Articles();
    $form = $this->createForm('AppBundle\Form\ArticlesType', $article);
    $form->handleRequest($request);
    $aujourdhui = new Datetime (date('Y-M-d h:i:s'));
    $article->setDatepublication(  $aujourdhui);
    $article->setDatemiseajour( $aujourdhui);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($article);
      $em->flush();
      $this->addFlash('success', "L'article a été créé avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_articles_new');
      } else {
        return $this->redirectToRoute('admin_articles_index');
      }
    }

    return $this->render('admin/blog/new_edit.html.twig', [
      'article' => $article,
      'form' => $form->createView(),
      'article_link' => 'article_index',
    ]);
  }
  /**
     * Displays a form to edit an existing articles entity.
     *
     * @Route("/{id}/edit", name="admin_articles_edit", requirements={"slug": "[a-z0-9\-]*"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Articles $article
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, Articles $article) {

      $editForm = $this->createForm('AppBundle\Form\ArticlesType', $article);
      $editForm->handleRequest($request);
      $aujourdhui = new Datetime (date('Y-M-d h:i:s'));
      $article->setDatemiseajour( $aujourdhui);
      if ($editForm->isSubmitted() && $editForm->isValid()) {
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('warning', "L'article a été mis à jour avec succès");
        if (isset($_POST['saveAndAdd'])) {
          return $this->redirectToRoute('admin_articles_new');
        } else {
          return $this->redirectToRoute('admin_articles_index');
        }
      }

      return $this->render('admin/blog/new_edit.html.twig', [
        'article' => $article,
        'form' => $editForm->createView(),
        'article_link' => 'article_index',
      ]);
    }
  /**
   * Lists all Articles entities.
   *
   * @Route("/{id}", name="admin_articles_show")
   * @Method("GET")
   */
  public function show(Articles $articles) {
    return $this->render('admin/blog/show.html.twig', array(
      'articles' => $articles,
      'article_link' => 'admin_articles_list',
    ));
  }

  /**
   * Deletes a contact entity.
   *
   * @Route("/{id}/delete", name="admin_article_delete")
   * @param Request $request
   * @param Articles $article
   *
   * @return Response
   */
  public function delete(Request $request, Articles $article) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($article);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Article supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
