<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Rejoindre;
use AppBundle\Entity\Articles;
use AppBundle\Entity\Categories;
use AppBundle\Entity\Inscription;
use AppBundle\Entity\Inscriptionprepa;
use AppBundle\Entity\OptionApprentissage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Admin controller.
 *
 * @Route("admin")
 */
class AdminController extends Controller {
  /**
   * shows dashboad.
   *
   * @Route("/", name="admin_index")
   * @Method("GET")
   * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();
    $preinscriptionsRepo = $em->getRepository(Inscription::class);
    $preinscriptions = $preinscriptionsRepo->findAll();

    $preinscriptionsprepaRepo = $em->getRepository(Inscriptionprepa::class);
    $preinscriptionsprepa = $preinscriptionsprepaRepo->findAll();

    $articlesrepo = $em->getRepository(Articles::class);
    $rejoindresrepo = $em->getRepository(Rejoindre::class);
    $categoriesrepo = $em->getRepository(Categories::class);

    $articles = $articlesrepo->findAll();
    $rejoindres= $rejoindresrepo->findAll();
    $categories= $categoriesrepo->findAll();

    $filieres = $em->getRepository(Filiere::class)->findAll();

    $options = $em->getRepository(OptionApprentissage::class)->findAll();

    $messages = $em->getRepository(Contact::class)->findAll();

    $statValues = $preinscriptionsRepo->getUsersByWeekPerMonth(date("Y"), date("m"));

    setlocale(LC_TIME, "fr_FR.UTF-8");
    $currenMonthFR = ucfirst(strftime("%B %Y", strtotime(date("Y-m-d"))));

    $year = date("Y", time());
    $month = intval(date("m", time()));
    $filterPeriods = array();
    while ($month > 0) {
      $filterPeriods[] = $year . '-' . $month . '-01';
      --$month;
    }
    foreach ($filterPeriods as $key => $value) {
      $filterPeriods[$key] = array(
        'val' => $value,
        'text' => ucfirst(strftime("%B %Y", strtotime($value))),
      );
    }

    $filterPeriods[] = array(
      'val' => $year,
      'text' => "Année en cours",
    );

    return $this->render('admin/index.html.twig', [
      'paliers' => array(),
      'preinscriptions' => count($preinscriptions),
      'preinscriptionsprepa' => count($preinscriptionsprepa),
      'filieres' => count($filieres),
      'articles' => count($articles),
      'categories' => count($categories),
      'options' => count($options),
      'rejoindres' => count($rejoindres),
      'messages' => count($messages),
      'currenMonthFR' => $currenMonthFR,
      'filterPeriods' => $filterPeriods,
      'year' => $year,
      'login_statistic' => json_encode($statValues),
    ]);
  }

  /**
   * @Route("/admin_ajax", name="admin_ajax_link")
   */
  public function adminAjax(Request $request) {
    $period = $request->request->get('period');
    $dt = date("m", strtotime($period));
    $time = strtotime($period);
    $year = date("Y", $time);
    $month = date("m", $time);
    $em = $this->getDoctrine()->getManager();
    $userRepo = $em->getRepository(Inscription::class);
    if ($period == date("Y", time())) {
      $statValues = $userRepo->getUsersByMonthPerYear();
    } else {
      $statValues = $userRepo->getUsersByWeekPerMonth($year, $month);
    }
    return new JsonResponse($statValues);
  }

  /**
   *
   *
   * @Route("/{period}/period", name="admin_period_statistic")
   * @Method("GET")
   * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
   * @param $period
   * @param DateService $service
   * @return Response
   */
  public function stats(Request $request, $period) {
    $em = $this->getDoctrine()->getManager();
    $userRepo = $em->getRepository(Inscription::class);
    $month = $request->query->get("month");

    $period = intval($period);
    $users = [];
    $info = '';

    if ($period < 4) {
      $dt = date("Y-m", strtotime($month));
      setlocale(LC_TIME, "fr_FR.UTF-8");
      $monthFR = ucfirst(strftime("%B %Y", strtotime(date("Y-m-d", strtotime($month)))));
      if ($period == 0) {
        $info = "Semaine 1 (du 01 au 07) de " . $monthFR;
        $users = $userRepo->getUsersByWeekPerMonthResults(new \DateTime("{$dt}-01"), new \DateTime("{$dt}-08"));
      } else if ($period == 1) {
        $info = "Semaine 2 (du 08 au 14) de " . $monthFR;
        $users = $userRepo->getUsersByWeekPerMonthResults(new \DateTime("{$dt}-08"), new \DateTime("{$dt}-15"));
      } else if ($period == 2) {
        $info = "Semaine 3 (du 15 au 21) de " . $monthFR;
        $users = $userRepo->getUsersByWeekPerMonthResults(new \DateTime("{$dt}-15"), new \DateTime("{$dt}-22"));
      } else if ($period == 3) {
        $info = "Semaine 4 (du 22 au 28/29/30/31) de " . $monthFR;
        $users = $userRepo->getUsersByWeekPerMonthResults(new \DateTime("{$dt}-22"), new \DateTime("{$dt}-30"));
      }
    } else {
      $t = $period - 3;
      $y = date("Y");
      $yy = $y . '-' . $t . '-01';
      $users = $userRepo->getUsersByMonthPerYearResults($period);
      setlocale(LC_TIME, "fr_FR.UTF-8");
      $info = ucfirst(strftime("%B %Y", strtotime(date("Y-m-d", strtotime($yy)))));
    }
    return $this->render('admin/inscription/index.html.twig', [
      'inscriptions' => $users,
      'info' => $info,
    ]);
  }

  /**
   * shows admin_inscription_index_per_filiere.
   *
   * @Route("/admin_inscription_index_per_filiere/inscriptoin/{id}/", name="admin_inscription_index_per_filiere")
   * @Method("GET")
   */
  public function admin_inscription_index_per_filiere() {

  }

}
