<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\SearchIndex;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * SearchIndex controller.
 *
 * @Route("admin/searchindexes")
 */
class AdminSearchIndexController extends Controller {
  /**
   * Lists all SearchIndex entities.
   *
   * @Route("/", name="admin_searchindexes_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $searchindexes = $em->getRepository('AppBundle:SearchIndex')->findAll();

    return $this->render('admin/searchindex/index.html.twig', array(
      'searchindexes' => $searchindexes,
      'searchindex_link' => 'searchindex_index',
    ));
  }

  /**
   * Lists all SearchIndex entities.
   *
   * @Route("/admin_inscription_index_per_searchindex/{id}", name="admin_inscription_index_per_searchindex")
   * @Method("GET")
   */
  public function admin_inscription_index_per_searchindex() {
    $em = $this->getDoctrine()->getManager();

    $searchindexes = $em->getRepository('AppBundle:SearchIndex')->findAll();

    return $this->render('admin/searchindex/index.html.twig', array(
      'searchindexes' => $searchindexes,
      'searchindex_link' => 'searchindex_index',
    ));
  }

  /**
   * Creates a new option d'apprentissage entity.
   *
   * @Route("/new", name="admin_searchindexes_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function create(Request $request) {
    $searchindex = new SearchIndex();
    $form = $this->createForm('AppBundle\Form\SearchIndexType', $searchindex);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($searchindex);
      $em->flush();
      $this->addFlash('success', "L'index a été créé avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_searchindexes_new');
      } else {
        return $this->redirectToRoute('admin_searchindexes_index');
      }
    }

    return $this->render('admin/searchindex/new_edit.html.twig', [
      'searchindex' => $searchindex,
      'form' => $form->createView(),
      'searchindex_link' => 'searchindex_index',
    ]);
  }

  /**
   * Displays a form to edit an existing searchindex entity.
   *
   * @Route("/{id}/edit", name="admin_searchindexes_edit")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @param SearchIndex $searchindex
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function update(Request $request, SearchIndex $searchindex) {

    $editForm = $this->createForm('AppBundle\Form\SearchIndexType', $searchindex);
    $editForm->handleRequest($request);

    if ($editForm->isSubmitted() && $editForm->isValid()) {
      $this->getDoctrine()->getManager()->flush();
      $this->addFlash('warning', "L'index a été mis à jour avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_searchindexes_new');
      } else {
        return $this->redirectToRoute('admin_searchindexes_index');
      }
    }

    return $this->render('admin/searchindex/new_edit.html.twig', [
      'searchindex' => $searchindex,
      'form' => $editForm->createView(),
      'searchindex_link' => 'searchindex_index',
    ]);
  }

  /**
   * Deletes a searchindex entity.
   *
   * @Route("/{id}/delete", name="admin_searchindexes_delete")
   * @param Request $request
   * @param SearchIndex $searchindex
   *
   * @return Response
   */
  public function delete(Request $request, SearchIndex $searchindex) {
    try {
      $entityManager = $this->getDoctrine()->getManager();

      $entityManager->remove($searchindex);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Index supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }
}
