<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contact controller.
 *
 * @Route("admin/contacts")
 */
class AdminContactController extends Controller {
  /**
   * Lists all Contact entities.
   *
   * @Route("/", name="admin_contacts_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $contacts = $em->getRepository('AppBundle:Contact')->findAll();

    return $this->render('admin/contact/index.html.twig', array(
      'contacts' => $contacts,
      'contact_link' => 'contact_index',
    ));
  }

  /**
   * Lists all Contact entities.
   *
   * @Route("/{id}", name="admin_contacts_show")
   * @Method("GET")
   */
  public function show(Contact $contact) {
    return $this->render('admin/contact/show.html.twig', array(
      'contact' => $contact,
      'contact_link' => 'contact_index',
    ));
  }

  /**
   * Deletes a contact entity.
   *
   * @Route("/{id}/delete", name="admin_contacts_delete")
   * @param Request $request
   * @param Contact $contact
   *
   * @return Response
   */
  public function delete(Request $request, Contact $contact) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($contact);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Message supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
