<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Inscriptionprepa;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * InscriptionPrepa controller.
 *
 * @Route("admin/inscriptions-prepa")
 */
class AdminInscriptionprepaController extends Controller {
  /**
   * Lists all Inscription entities.
   *
   * @Route("/", name="admin_inscriptionsprepa_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $inscriptionsprepa = $em->getRepository('AppBundle:Inscriptionprepa')->findAll();

    return $this->render('admin/inscriptionprepa/index.html.twig', array(
      'inscriptionsprepa' => $inscriptionsprepa,
      'inscriptionprepa_link' => 'inscriptionprepa_index',
    ));
  }

  /**
   * Lists all Inscription entities.
   *
   * @Route("/{id}", name="admin_inscriptionsprepa_show")
   * @Method("GET")
   */
  public function show(Inscriptionprepa $inscriptionprepa) {
    return $this->render('admin/inscriptionprepa/show.html.twig', array(
      'inscriptionprepa' => $inscriptionprepa,
      'inscriptionprepa_link' => 'inscriptionprepa_index',
    ));
  }

  /**
   * Deletes a inscription entity.
   *
   * @Route("/{id}/delete", name="admin_inscriptionsprepa_delete")
   * @param Request $request
   * @param Inscriptionprepa $inscription
   *
   * @return Response
   */
  public function delete(Request $request, Inscriptionprepa $inscription) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($inscription);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Pré-inscription supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
