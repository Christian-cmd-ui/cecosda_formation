<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\OptionApprentissage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * OptionApprentissage controller.
 *
 * @Route("admin/optionapprentissages")
 */
class AdminOptionApprentissageController extends Controller {
  /**
   * Lists all OptionApprentissage entities.
   *
   * @Route("/", name="admin_optionapprentissages_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $optionapprentissages = $em->getRepository('AppBundle:OptionApprentissage')->findAll();

    return $this->render('admin/optionapprentissage/index.html.twig', array(
      'optionapprentissages' => $optionapprentissages,
      'optionapprentissage_link' => 'optionapprentissage_index',
    ));
  }

  /**
   * Lists all OptionApprentissage entities.
   *
   * @Route("/admin_inscription_index_per_optionapprentissage/{id}", name="admin_inscription_index_per_optionapprentissage")
   * @Method("GET")
   */
  public function admin_inscription_index_per_optionapprentissage() {
    $em = $this->getDoctrine()->getManager();

    $optionapprentissages = $em->getRepository('AppBundle:OptionApprentissage')->findAll();

    return $this->render('admin/optionapprentissage/index.html.twig', array(
      'optionapprentissages' => $optionapprentissages,
      'optionapprentissage_link' => 'optionapprentissage_index',
    ));
  }

  /**
   * Creates a new option d'apprentissage entity.
   *
   * @Route("/new", name="admin_optionapprentissages_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function create(Request $request) {
    $optionapprentissage = new OptionApprentissage();
    $form = $this->createForm('AppBundle\Form\OptionApprentissageType', $optionapprentissage);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($optionapprentissage);
      $em->flush();
      $this->addFlash('success', "L'option d'apprentissage a été créé avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_optionapprentissages_new');
      } else {
        return $this->redirectToRoute('admin_optionapprentissages_index');
      }
    }

    return $this->render('admin/optionapprentissage/new_edit.html.twig', [
      'optionapprentissage' => $optionapprentissage,
      'form' => $form->createView(),
      'optionapprentissage_link' => 'optionapprentissage_index',
    ]);
  }

  /**
   * Displays a form to edit an existing optionapprentissage entity.
   *
   * @Route("/{id}/edit", name="admin_optionapprentissages_edit")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @param OptionApprentissage $optionapprentissage
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function update(Request $request, OptionApprentissage $optionapprentissage) {

    $editForm = $this->createForm('AppBundle\Form\OptionApprentissageType', $optionapprentissage);
    $editForm->handleRequest($request);

    if ($editForm->isSubmitted() && $editForm->isValid()) {
      $this->getDoctrine()->getManager()->flush();
      $this->addFlash('warning', "La filière a été mis à jour avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_optionapprentissages_new');
      } else {
        return $this->redirectToRoute('admin_optionapprentissages_index');
      }
    }

    return $this->render('admin/optionapprentissage/new_edit.html.twig', [
      'optionapprentissage' => $optionapprentissage,
      'form' => $editForm->createView(),
      'optionapprentissage_link' => 'optionapprentissage_index',
    ]);
  }

  /**
   * Deletes a optionapprentissage entity.
   *
   * @Route("/{id}/delete", name="admin_optionapprentissages_delete")
   * @param Request $request
   * @param OptionApprentissage $optionapprentissage
   *
   * @return Response
   */
  public function delete(Request $request, OptionApprentissage $optionapprentissage) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $inscriptions = $optionapprentissage->getInscriptions();
      if (count($inscriptions) > 0) {
        return new Response(json_encode([
          'message' => 'Une inscription est associé à cette option d\'apprentissage. Bien vouloir supprimer cette inscription avant de continuer.',
          'status' => 'failed',
        ]));
      }
      $entityManager->remove($optionapprentissage);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Option d'apprentissage supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }
}
