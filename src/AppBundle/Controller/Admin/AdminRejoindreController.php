<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Rejoindre;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Inscription controller.
 *
 * @Route("admin/rejoindre")
 */
class AdminRejoindreController extends Controller {
  /**
   * Lists all Inscription entities.
   *
   * @Route("/", name="admin_rejoindres_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $rejoindres = $em->getRepository('AppBundle:Rejoindre')->findAll();

    return $this->render('admin/rejoindre/index.html.twig', array(
      'rejoindres' => $rejoindres,
      'rejoindres_link' => 'rejoindres_index',
    ));
  }

  /**
   * Lists all rejoindre entities.
   *
   * @Route("/{id}", name="admin_rejoindre_show")
   * @Method("GET")
   */
  public function show(Rejoindre $rejoindre) {
    return $this->render('admin/rejoindre/show.html.twig', array(
      'rejoindre' => $rejoindre,
      'rejoindre_link' => 'rejoindre_index',
    ));
  }

  /**
   * Deletes a inscription entity.
   *
   * @Route("/{id}/delete", name="admin_rejoindres_delete")
   * @param Request $request
   * @param Rejoindre $rejoindre
   *
   * @return Response
   */
  public function delete(Request $request, Rejoindre $rejoindre) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($rejoindre);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Votre candidat a été supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
