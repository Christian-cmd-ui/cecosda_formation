<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Filiere;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Filiere controller.
 *
 * @Route("admin/filieres")
 */
class AdminFiliereController extends Controller {
  /**
   * Lists all filiere entities.
   *
   * @Route("/", name="admin_filieres_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $filieres = $em->getRepository('AppBundle:Filiere')->findAll();

    return $this->render('admin/filiere/index.html.twig', array(
      'filieres' => $filieres,
      'filiere_link' => 'filiere_index',
    ));
  }

  /**
   * Creates a new filiere entity.
   *
   * @Route("/new", name="admin_filieres_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function create(Request $request) {
    $filiere = new Filiere();
    $form = $this->createForm('AppBundle\Form\FiliereType', $filiere);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($filiere);
      $em->flush();
      $this->addFlash('success', "La filière a été créé avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_filieres_new');
      } else {
        return $this->redirectToRoute('admin_filieres_index');
      }
    }

    return $this->render('admin/filiere/new_edit.html.twig', [
      'filiere' => $filiere,
      'form' => $form->createView(),
      'filiere_link' => 'filiere_index',
    ]);
  }

  /**
   * Displays a form to edit an existing filiere entity.
   *
   * @Route("/{id}/edit", name="admin_filieres_edit", requirements={"slug": "[a-z0-9\-]*"})
   * @Method({"GET", "POST"})
   * @param Request $request
   * @param Filiere $filiere
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function update(Request $request, Filiere $filiere) {

    $editForm = $this->createForm('AppBundle\Form\FiliereType', $filiere);
    $editForm->handleRequest($request);

    if ($editForm->isSubmitted() && $editForm->isValid()) {
      $this->getDoctrine()->getManager()->flush();
      $this->addFlash('warning', "La filière a été mis à jour avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_filieres_new');
      } else {
        return $this->redirectToRoute('admin_filieres_index');
      }
    }

    return $this->render('admin/filiere/new_edit.html.twig', [
      'filiere' => $filiere,
      'form' => $editForm->createView(),
      'filiere_link' => 'filiere_index',
    ]);
  }

  /**
   * Deletes a student entity.
   *
   * @Route("/{id}/delete", name="admin_filieres_delete")
   * @param Request $request
   * @param Filiere $filiere
   *
   * @return Response
   */
  public function delete(Request $request, Filiere $filiere) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $inscriptions = $filiere->getInscriptions();
      if (count($inscriptions) > 0) {
        return new Response(json_encode([
          'message' => 'Une inscription est associé à cette filière. Bien vouloir supprimer cette inscription avant de continuer.',
          'status' => 'failed',
        ]));
      }
      $entityManager->remove($filiere);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Filiere supprimé avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }
}
