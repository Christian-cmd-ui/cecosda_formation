<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Destinataire;
use AppBundle\Form\DestinataireType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Destinataire controller.
 *
 * @Route("admin/destinataires")
 */
class AdminDestinataireController extends Controller
{
    /**
     * Lists all destinataire entities.
     *
     * @Route("/", name="admin_destinataires_index")
     * @Method("GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $destinataires = $em->getRepository('AppBundle:Destinataire')->findAll();

        return $this->render('admin/destinataire/index.html.twig', array(
            'destinataires' => $destinataires,
            'destinataire_link' => 'destinataire_index',
        ));
    }

    /**
     * Creates a new destinataire entity.
     *
     * @Route("/new", name="admin_destinataires_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $destinataire = new Destinataire();
        $form = $this->createForm(DestinataireType::class, $destinataire);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($destinataire);
            $em->flush();
            $this->addFlash('success', 'La catégorie de destinataire été créé avec succès');
            if (isset($_POST['saveAndAdd'])) {
                return $this->redirectToRoute('admin_destinataires_new');
            }else{
                return $this->redirectToRoute('admin_destinataires_index');
            }
        }

        return $this->render('admin/destinataire/new_edit.html.twig', [
            'destinataire' => $destinataire,
            'form' => $form->createView(),
            'destinataire_link' => 'destinataire_index',
        ]);
    }

    /**
     * Displays a form to edit an existing destinataire entity.
     *
     * @Route("/{id}/{slug}/edit", name="admin_destinataires_edit", requirements={"slug": "[a-z0-9\-]*"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Destinataire $destinataire
     * @param $slug
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, Destinataire $destinataire, $slug)
    {
        if ($destinataire->getSlug() !== $slug){
            return $this->redirectToRoute('admin_destinataires_edit', [
                'id' => $destinataire->getId(),
                'slug' => $destinataire->getSlug()
            ], 301);
        }

        $editForm = $this->createForm('AppBundle\Form\DestinataireType', $destinataire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La catégorie de destinataire été mis à jour avec succès');
            if (isset($_POST['saveAndAdd'])) {
                return $this->redirectToRoute('admin_destinataires_new');
            }else{
                return $this->redirectToRoute('admin_destinataires_index');
            }
        }

        return $this->render('admin/destinataire/new_edit.html.twig', [
            'destinataire' => $destinataire,
            'form' => $editForm->createView(),
            'destinataire_link' => 'destinataire_index',
        ]);
    }

    /**
     * Deletes a student entity.
     *
     * @Route("/{id}/delete", name="admin_destinataires_delete")
     * @param Request $request
     * @param Destinataire $destinataire
     * @return Response
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Destinataire $destinataire)
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($destinataire);
                $entityManager->flush();
                return new Response(json_encode([
                    'message' => "Destinataire supprimé avec succès",
                    'status' => 'success',
                ]));
            } catch (\Exception $e) {
                return new Response(json_encode([
                    'message' => $e->getMessage(),
                    'status' => 'failed',
                ]));
            }
        }
    }
}
