<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Categories;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contact controller.
 *
 * @Route("admin/categories")
 */
class AdminCategoriesController extends Controller {
  /**
   * Lists all Contact entities.
   *
   * @Route("/", name="admin_categories_index")
   * @Method("GET")
   */
  public function index() {
    $em = $this->getDoctrine()->getManager();

    $Categories = $em->getRepository('AppBundle:Categories')->findBy([], ['id' => "DESC"]);;

    return $this->render('admin/categories/index.html.twig', array(
      'categories' => $Categories
    ));
  }
 /**
   * Creates a new Categories entity.
   *
   * @Route("/new", name="admin_categories_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function create(Request $request) {
    $Categorie = new Categories();
    $form = $this->createForm('AppBundle\Form\CategoriesType', $Categorie);
    $form->handleRequest($request);
  
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($Categorie);
      $em->flush();
      $this->addFlash('success', "La Categorie a été créée avec succès");
      if (isset($_POST['saveAndAdd'])) {
        return $this->redirectToRoute('admin_categories_new');
      } else {
        return $this->redirectToRoute('admin_categories_index');
      }
    }

    return $this->render('admin/categories/new_edit.html.twig', [
      'categorie' => $Categorie,
      'form' => $form->createView(),
      'categorie_link' => 'Categorie_index',
    ]);
  }
  /**
     * Displays a form to edit an existing Categories entity.
     *
     * @Route("/{id}/edit", name="admin_categories_edit", requirements={"slug": "[a-z0-9\-]*"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Categories $Categorie
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, Categories $Categorie) {

      $editForm = $this->createForm('AppBundle\Form\CategoriesType', $Categorie);
      $editForm->handleRequest($request);
     
      if ($editForm->isSubmitted() && $editForm->isValid()) {
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('warning', "La Categorie a été mise à jour avec succès");
        if (isset($_POST['saveAndAdd'])) {
          return $this->redirectToRoute('admin_categories_new');
        } else {
          return $this->redirectToRoute('admin_categories_index');
        }
      }

      return $this->render('admin/categories/new_edit.html.twig', [
        'categorie' => $Categorie,
        'form' => $editForm->createView(),
        'categorie_link' => 'categorie_index',
      ]);
    }
  /**
   * Lists all Categories entities.
   *
   * @Route("/{id}", name="admin_categories_show")
   * @Method("GET")
   */
  public function show(Categories $Categories) {
    return $this->render('admin/categories/show.html.twig', array(
      'categories' => $Categories,
      'categorie_link' => 'admin_categories_list',
    ));
  }

  /**
   * Deletes a contact entity.
   *
   * @Route("/{id}/delete", name="admin_categorie_delete")
   * @param Request $request
   * @param Categories $Categorie
   *
   * @return Response
   */
  public function delete(Request $request, Categories $Categorie) {
    try {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($Categorie);
      $entityManager->flush();
      return new Response(json_encode([
        'message' => "Categorie supprimée avec succès",
        'status' => 'success',
      ]));
    } catch (\Exception $e) {
      return new Response(json_encode([
        'message' => $e->getMessage(),
        'status' => 'failed',
      ]));
    }
  }

}
