<?php

namespace AppBundle\Controller;


use AppBundle\Entity\SearchIndex;
use AppBundle\Service\FileUploader;
use Doctrine\ORM\Mapping\Id;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends Controller {



    public function showAction($id) {

        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('AppBundle:Articles')->findOneBy(["id" => $id]);
        $categories = $em->getRepository('AppBundle:Categories')->findOneBy(["id" => $id]);
      
        // replace this example code with whatever you need
        return $this->render('default/articles.html.twig', [
          'article' => $article,
          'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);

    }
}