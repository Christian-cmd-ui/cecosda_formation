<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;

use AppBundle\Entity\Emails;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Rejoindre;
use AppBundle\Entity\Inscription;
use AppBundle\Entity\SearchIndex;
use AppBundle\Service\FileUploader;
use AppBundle\Entity\Inscriptionprepa;
use AppBundle\Service\EmailSender;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller {
  
  
  public $success;
  /**
    * @Route(
    *   "/{locale}", 
    *   defaults={
    *       "locale": "fr",
    *   },
    *   requirements={
    *       "locale": "en|fr",
    *   },
    *   name="homepage"
    * )
    */
  public function indexAction(Request $request) {

    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();

    $filieres = $em->getRepository('AppBundle:Filiere')->findBy([], ['id' => "DESC"]);

    return $this->render('home.html.twig', [
           'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
           'currentroute' => $routeName,
           'filieres' => $filieres,
    ]);
  }

  /**
   * @Route("/contact", name="contact")
   * @Method("POST")
   */
  public function contactAction(Request $request) {

    $contact = new Contact();
    $routeName = $request->attributes->get('_route');

    $form = $this->createForm('AppBundle\Form\ContactType', $contact);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($contact);
      $em->flush();
      $params = array(
        'nom' => $contact->getNom(),
        'email' => $contact->getEmail(),
        'objet' => $contact->getObjet(),
        'message' => $contact->getMessage(),
      );
      $this->sendContactMailToAdmin($params, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      return $this->render('default/contactsuccess.html.twig', [
        'currentroute' => $routeName,

      ]);
    }

    return $this->redirectToRoute('homepage');
  }

  /**
   * @Route("/assistants-administratifs", name="assistantsadministratifs")
   */
  public function cqpAction(Request $request) {
    // replace this example code with whatever you need
    $routeName = $request->attributes->get('_route');
    return $this->render('default/cqp.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

  /**
   * @Route("/{_locale}/inscription-prepa",
   *  defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   },  name="inscriptionprepa")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
   * 
   * 
   *  
   */
  public function inscriptionprepaAction(Request $request, FileUploader $fileUploader, $_locale) {

    $request->setLocale($_locale);
    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();

    $success = false;
    $inscription = new Inscriptionprepa();
    $form = $this->createForm('AppBundle\Form\InscriptionprepaType', $inscription);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      
      
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      
    }

    // replace this example code with whatever you need
    return $this->render('default/inscriptionprepa.html.twig', [
      'currentroute' => $routeName,
      'inscription' => $inscription,
      'form' => $form->createView(),
      'success' => $success,
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
    ]);

  }
/**
   * @Route("/{_locale}/inscription",
   *  defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   },  name="inscription")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
   * 
   * 
   *  
   */
  public function inscriptionAction(Request $request, FileUploader $fileUploader, $_locale) {

    $request->setLocale($_locale);
    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();

    $success = false;
    $fileValid = true;
    $inscription = new Inscription();
    $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
     
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      
    }

    // replace this example code with whatever you need
    return $this->render('default/preinscripton.html.twig', [
      'currentroute' => $routeName,
      'inscription' => $inscription,
      'form' => $form->createView(),
      'success' => $success,
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
    ]);

  }

  /**
   * @Route("/inscription",
   *  
    *    name="preinscription")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
   * 
   * 
   *  
   */
  public function preinscriptionAction(Request $request, FileUploader $fileUploader) {

 
    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();

    $success = false;
    $fileValid = true;
    $inscription = new Inscription();
    $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $cni = $form['cni']->getData();
      $cv = $form['cv']->getData();
      $dernierDiplome = $form['dernierDiplome']->getData();
      $extensions = array('png', 'pdf', 'jpeg', 'jpg', 'doc', 'docx');
      if ($cni && !in_array($cni->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier CNI invalide');
        $fileValid = false;
      } else if ($dernierDiplome && !in_array($dernierDiplome->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier Diplôme invalide');
        $fileValid = false;
      } 
      
      if ($fileValid) {
        if ($cni) {
          $fileName = $fileUploader->upload($cni, 'cni');
          $inscription->setCni($fileName);
        }

           if ($dernierDiplome) {
          $fileName2 = $fileUploader->upload($dernierDiplome, 'diplome');
          $inscription->setDernierDiplome($fileName2);
        }
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      }
    }

    // replace this example code with whatever you need
    return $this->render('default/preinscripton.html.twig', [
      'currentroute' => $routeName,
      'inscription' => $inscription,
      'form' => $form->createView(),
      'success' => $success,
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
    ]);

  }

  /**
   * @param $user
   */
  public function sendMailToUser($user) {
    try {
      $message = \Swift_Message::newInstance()
        ->setFrom(['infos@cecosdaformation.com' => 'CECOSDAFormation - Au delà du diplôme, la compétence'])
        ->setTo($user->getEmail())
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject("[CECOSDAFormation] Pré-inscription enregistré")
        ->setBody($this->renderView('mail/mail.html.twig', [
          'user' => $user,
        ]));
    $this->get('mailer')->send($message);
    } catch (\Throwable $th) {}
  }

  /**
   * @param $user
   */
  public function sendMailToAdmin($user, $adminEmail) {
    try {
      $message = \Swift_Message::newInstance()
        ->setFrom(['infos@cecosdaformation.com' => 'CECOSDAFormation - Au delà du diplôme, la compétence'])
        ->setTo($adminEmail)
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject("[CECOSDAFormation] Pré-inscription enregistré")
        ->setBody($this->renderView('mail/admin/mail.html.twig', [
          'user' => $user,
        ]));
      $this->get('mailer')->send($message);
    } catch (\Throwable $th) {}
  }
  /**
   * @param $user
   */
  public function sendMailForVacation($user) {
    try {
      $message = \Swift_Message::newInstance()
        ->setFrom(['infos@cecosdaformation.com' => 'CECOSDAFormation - Au delà du diplôme, la compétence'])
        ->setTo($user->getEmail())
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject("[CECOSDAFormation] - Demande de Vacation")
        ->setBody($this->renderView('mail/mailvacation.html.twig', [
          'user' => $user,
        ]));
      $this->get('mailer')->send($message);
    } catch (\Throwable $th) {}
  }

  /**
   * @param $user
   */
  public function sendMailForAdmin($user, $adminEmail) {
    try {
      $message = \Swift_Message::newInstance()
        ->setFrom(['infos@cecosdaformation.com' => 'CECOSDAFormation - Au delà du diplôme, la compétence'])
        ->setTo($adminEmail)
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject("[CECOSDAFormation] - Demande de Vacation")
        ->setBody($this->renderView('mail/admin/mailvacation.html.twig', [
          'user' => $user,
        ]));
      $this->get('mailer')->send($message);
    } catch (\Throwable $th) {}
  }

  /**
   * @param $user
   */
  public function sendContactMailToAdmin($params, $adminEmail) {
    /*try {
  $message = \Swift_Message::newInstance()
  ->setFrom(['no-reply@cecosdaformation.com' => 'CECOSDAFormation - Au delà du diplôme, la compétence'])
  ->setTo($adminEmail)
  ->setCharset('utf-8')
  ->setContentType('text/html')
  ->setSubject("[CECOSDAFormation] Nouveau message envoyé")
  ->setBody($this->renderView('mail/admin/mailcontact.html.twig', array(
  'nom' => $params['nom'],
  'email' => $params['email'],
  'objet' => $params['objet'],
  'message' => $params['message'],
  )));
  $this->get('mailer')->send($message);
  } catch (\Throwable $th) {}*/
  }

  

  /**
   * @Route("/{_locale}/blog", 
   *   defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   },  name="blogpost")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
   * 
   * 
   * 
   */
  public function BlogAction(Request $request, $_locale) {

    $request->setLocale($_locale);
    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();

    if($_locale == "fr") {

      $articles = $em->getRepository('AppBundle:Articles')->findBy( ["langue" => "1"], ['id' => 'DESC']);
      $categories = $em->getRepository('AppBundle:Categories')->findBy( ["langue" => "fr"], ['id' => 'DESC']);
      
    }

    if($_locale == "en") {
 
      $articles = $em->getRepository('AppBundle:Articles')->findBy( ["langue" => "0"], ['id' => 'DESC']);
      $categories = $em->getRepository('AppBundle:Categories')->findBy( ["langue" => "en"], ['id' => 'DESC']);

    }


    // replace this example code with whatever you need
    return $this->render('default/blogpost.html.twig', [
      'articles' => $articles,
      'currentroute' => $routeName,
      'categories' => $categories,
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
    ]);
  }
  /**
   * @Route("/{_locale}/rejoindre",
   *  
   *   * defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   },  name="rejoindre")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
    *
   */
  public function RejoindreAction(Request $request, FileUploader $fileUploader, $_locale) {

    $request->setLocale($_locale);
    $routeName = $request->attributes->get('_route');
    $em = $this->getDoctrine()->getManager();
    $success = false;
    $fileValid = true;
    $rejoindre = new Rejoindre();
    $form = $this->createForm('AppBundle\Form\RejoindreType', $rejoindre);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $motivation = $form['motivation']->getData();
      $cv = $form['cv']->getData();
      $dernierDiplome = $form['dernierDiplome']->getData();
      $extensions = array('png', 'pdf', 'jpeg', 'jpg', 'doc', 'docx');
      if ($motivation && !in_array($motivation->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Lettre de motivation invalide');
        $fileValid = false;
      } else if ($dernierDiplome && !in_array($dernierDiplome->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier Diplôme invalide');
        $fileValid = false;
      } else if ($cv && !in_array($cv->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier CV invalide');
        $fileValid = false;
      }
      if ($fileValid) {
        if ($motivation) {
          $fileName = $fileUploader->upload($motivation, 'motivation');
          $rejoindre->setMotivation($fileName);
        }

        if ($cv) {
          $fileName3 = $fileUploader->upload($cv, 'cv');
          $rejoindre->setCv($fileName3);
        }

        if ($dernierDiplome) {
          $fileName2 = $fileUploader->upload($dernierDiplome, 'diplome');
          $rejoindre->setDernierDiplome($fileName2);
        }
        $em->persist($rejoindre);
        $em->flush();

        $success = true;
        $this->sendMailForVacation($rejoindre);
        $this->sendMailForAdmin($rejoindre, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      }
    }

    // replace this example code with whatever you need
    return $this->render('default/rejoindre.html.twig', [
      'rejoindre' => $rejoindre,
      'currentroute' => $routeName,
      'form' => $form->createView(),
      'success' => $success,
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
    ]);
  }

  /**
   * @Route("/{_locale}/cgu", 
   *    defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   }, name="cgu")
    *
    * @param Request $request
    * @param $_locale
    * @return Response
    *
    *
   */
  public function cguAction(Request $request, $_locale) {
   //Initialisation de la langue à celle choisie par le user
   $routeName = $request->attributes->get('_route');
   $request->setLocale($_locale);
    return $this->render('default/cgu.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

  /**
   * @Route("/search", name="search")
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function performSearch(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $repo = $em->getRepository(SearchIndex::class);
    $data = $repo->search($request->query->get("q"));
    $results = \array_map(function ($d) {
      return $d->toArray();
    }, $data);
    return new JsonResponse([
      'status' => 'ok',
      'data' => $results,
    ]);
  }

  /**
   * @Route("/rechercher", name="rechercher")
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function showSearch(Request $request) {
    $routeName = $request->attributes->get('_route');
    return $this->render('default/rechercher.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

//  /*  /**
//    * @Route("/nos-formations-BEPC-Probatoire-BAC", name="formationscourtes")
//    * @return \Symfony\Component\HttpFoundation\Response
//    */
//   public function courtesAction() {
//     return $this->render('default/formationscourtes.html.twig', [
//       'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
//       'currentroute' => $routeName,
//     ]);
//   }

//   /**
//    * @Route("/nos-ateliers-de-formation", name="ateliers")
//    * @return \Symfony\Component\HttpFoundation\Response
//    */
//   public function ateliersAction() {
//     return $this->render('default/ateliers.html.twig', [
//       'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
//     ]);
//   }

//     /**
//     * @Route("/{_locale}/marketing-et-communication",
//     * defaults={
//     *       "_locale": "fr",
//     *   },
//     *   requirements={
//     *       "_locale": "en|fr",
//     *   }, name="marketing_communication")
//     *
//     * @param Request $request
//     * @param $_locale
//     * @return Response
//     *
//     *
//     *
//    */
//   public function marketingAction(Request $request, $_locale) {

//     //Initialisation de la langue à celle choisie par le user
//     $request->setLocale($_locale);
//     // replace this example code with whatever you need
//     return $this->render('default/marketing_communication.html.twig', [
//       'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
//     ]);
//   }

//     /**
//    * @Route("/{_locale}/gestion-et-management",
//     * defaults={
//     *       "_locale": "fr",
//     *   },
//     *   requirements={
//     *       "_locale": "en|fr",
//     *   }, name="gestion_management")
//      * @param Request $request
//     * @param $_locale
//     * @return Response
//    */
//   public function gestionAction(Request $request, $_locale) {
//       //Initialisation de la langue à celle choisie par le user
//       $request->setLocale($_locale);
//     // replace this example code with whatever you need
//     return $this->render('default/gestion_management.html.twig', [
//       'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
//     ]);
//   }

//   /**
//    * @Route("/{_locale}/techniques-informatique",
//     * defaults={
//     *       "_locale": "fr",
//     *   },
//     *   requirements={
//     *       "_locale": "en|fr",
//     *   }, name="informatique")
//      * @param Request $request
//     * @param $_locale
//     * @return Response
//    */
//   public function informatiqueAction(Request $request, $_locale) {
//       //Initialisation de la langue à celle choisie par le user
//       $request->setLocale($_locale);
//     // replace this example code with whatever you need
//     return $this->render('default/techniques_informatique.html.twig', [
//       'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
//     ]);
//   } */

  /**
   *  @Route("/{_locale}/formations-logiciels",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_logiciels")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
  public function logicielsAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    // replace this example code with whatever you need
    return $this->render('default/formations_logiciels.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

  /**
   *  @Route("/{_locale}/qui-sommes-nous",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="qui_sommes_nous")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function quisommesAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    // replace this example code with whatever you need
    return $this->render('default/qui_sommes_nous.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

  /**
   *  @Route("/{_locale}/nos-atouts",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="nos_atouts")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function atoutsAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    // replace this example code with whatever you need
    return $this->render('default/nos_atouts.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }  
  
  
  
  /**
   *  @Route("/{_locale}/deja-professionnels-du-metier",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="deja_professionnels")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function professionnelsAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    // replace this example code with whatever you need
    return $this->render('default/deja_professionnels.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

    
  /**
   *  @Route("/{_locale}/nos-formations-1-mois",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_1_mois")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function unmoisAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations-1-mois.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

      
  /**
   *  @Route("/{_locale}/nos-formations-3-mois",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_3_mois")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function troismoisAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations-3-mois.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

      
  /**
   *  @Route("/{_locale}/nos-formations-6-mois",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_6_mois")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function sixmoisAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations-6-mois.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
  
      
  /**
   *  @Route("/{_locale}/nos-formations-9-mois",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_9_mois")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function neufmoisAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations-9-mois.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
  
      
  /**
   *  @Route("/{_locale}/nos-formations-gratuites",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_free")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function freeAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations-free.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
      
  /**
   *  @Route("/{_locale}/formations-a-la-cartes",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_carte")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsCarteAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations_cartes.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
           
  /**
   *  @Route("/{_locale}/special-it",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_prepa")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsPrepaAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations_prepa.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }           
  /**
   *  @Route("/{_locale}/formations-vacances",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_vacances")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsvacancesAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/vacances.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
      
  /**
   *  @Route("/{_locale}/formations-entreprises",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_entreprise")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsEntreprisesAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations_entreprises.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }
      
  /**
   *  @Route("/{_locale}/formations-digital",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="formations_digital")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsdigitalAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations_digital.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }    
  
  
  /**
   *  @Route("/{_locale}/admission/CQP",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="admissionCQP")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function admissionCQPAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/admissionCQP.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }  
  
  /**
   *  @Route("/{_locale}/admission/SpecialIT",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="admissionSPI")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function admissionSPIAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/admissionSPI.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  } 
  
  /**
   *  @Route("/{_locale}/inscription/SpecialIT-05-08",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionSPIeveil")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionSPIeveilAction(Request $request, $_locale, EmailSender $mails) {
      //Initialisation de la langue à celle choisie par le user
      $success = false;
      $inscription = new Inscription();
      $inscription->setType("Eveil informatique");      
      $inscription->setPlagechoisie("08:00-11:30");
      $em = $this->getDoctrine()->getManager();
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);
     
    if ($form->isSubmitted() && $form->isValid()) {
      
        $em->persist($inscription);
        $em->flush();
        
        $mails->sendMails($inscription->getEmail(), "infos@cecosdaformation.com", "Nouvelle preinscription", $inscription ) ;
        $this->addFlash('info', 'Votre inscription a été enregistrée avec succès.');
        $success = true;
        return $this->redirectToRoute('inscriptionSPIeveil');
    }
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/inscriptionspieveil.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formeveil' => $form->createView(),
    ]);
  }
  
  
  /**
   *  @Route("/{_locale}/inscription/SpecialIT-08-10",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionSPIaventure")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionSPIaventureAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $inscription = new Inscription();
      $inscription->setType("Creativite Informatique ");      
      $inscription->setPlagechoisie("08:00-11:30");
      $em = $this->getDoctrine()->getManager();
      $success = false;
      $fileValid = true;
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
        $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
        return $this->redirectToRoute('inscriptionSPIaventure');
    }
    return $this->render('default/inscriptionspiaventure.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formaventure' =>$form->createView(),
    ]);
  }  
  
  /**
   *  @Route("/{_locale}/inscription/SpecialIT-10-13",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionSPIcreativity")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionSPIcreativityAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $inscription = new Inscription();
      $inscription->setType("Informatique Applique ");     
      $em = $this->getDoctrine()->getManager();
      $success = false;
      $fileValid = true;
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      
        $em->persist($inscription);
        $em->flush();
        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
        $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
        return $this->redirectToRoute('inscriptionSPIcreativity');
    }
    return $this->render('default/inscriptionspicreativity.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formcreativity' => $form->createView(),
    ]);
  }
  
  
  /**
   *  @Route("/{_locale}/inscription/SpecialIT-14+",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionSPIspecial")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionSPIspecialAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $success = false;
      $inscription = new Inscription();
      $inscription->setType("Informatique Specialise ");
      $em = $this->getDoctrine()->getManager();
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
     
        $em->persist($inscription);
        $em->flush();
        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
        $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
        return $this->redirectToRoute('inscriptionSPIspecial');
    }
    return $this->render('default/inscriptionspispecial.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formspecial' => $form->createView(),
    ]);
  }
  
  
  /**
   *  @Route("/{_locale}/inscription/SpecialIT-adultes",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionSPIadulte")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionSPIadultesAction(Request $request, $_locale, FileUploader $fileUploader) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $inscription = new Inscription();
      $em = $this->getDoctrine()->getManager();
      $success = false;
      $fileValid = true;
      $inscription->setType("Savoir informatique");
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $cni = $form['cni']->getData();
      $extensions = array('png', 'pdf', 'jpeg', 'jpg', 'doc', 'docx');
      if ($cni && !in_array($cni->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier CNI invalide');
        $fileValid = false;
      } 
      if ($fileValid) {
        if ($cni) {
          $fileName = $fileUploader->upload($cni, 'cni');
          $inscription->setCni($fileName);
        }

        
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $this->sendMailToUser($inscription);
        $this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      }
      $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
      return $this->redirectToRoute('inscriptionSPIadulte');
    }
    return $this->render('default/inscriptionspiadultes.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formadulte' => $form->createView(),
    ]);
  }
  
  
  /**
   *  @Route("/{_locale}/inscription/CQP-en-ligne",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionCQPenligne")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionCQPenligneAction(Request $request, $_locale, FileUploader $fileUploader, EmailSender $mails) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $inscription = new Inscription();
      $em = $this->getDoctrine()->getManager();
      $success = false;
      $fileValid = true;
      $inscription->setType("AD en ligne | online");
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $cni = $form['cni']->getData();
      
      $extensions = array('png', 'pdf', 'jpeg', 'jpg', 'doc', 'docx');
      if ($cni && !in_array($cni->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier CNI invalide');
        $fileValid = false;
      } 
      if ($fileValid) {
        if ($cni) {
          $fileName = $fileUploader->upload($cni, 'cni');
          $inscription->setCni($fileName);
        }
        
        $em->persist($inscription);
        $em->flush();

        $success = true;
        $mails->sendMails($inscription->getEmail(),"cecosdaformation@gmail.com","Nouvelle inscription",$inscription);
        //$this->sendMailToUser($inscription);
        //$this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      }
      $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
      return $this->redirectToRoute('inscriptionCQPenligne');
    }
    return $this->render('default/inscriptioncqpligne.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formligne' => $form->createView(),
    ]);
  }
      
  
  
  /**
   *  @Route("/{_locale}/inscription/CQP-en-presentiel",
  * defaults={
  *       "_locale": "fr",
  *   },
  *   requirements={
  *       "_locale": "en|fr",
    *   }, name="inscriptionCQPenpresentiel")

     * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function inscriptionCQPenpresentielAction(Request $request, $_locale, FileUploader $fileUploader,EmailSender $mails) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
      $inscription = new Inscription();
      $em = $this->getDoctrine()->getManager();
      $success = false;
      $fileValid = true;
      $inscription->setType("AD en ligne | online");
      $form = $this->createForm('AppBundle\Form\InscriptionType', $inscription);
      $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $cni = $form['cni']->getData();
      $extensions = array('png', 'pdf', 'jpeg', 'jpg', 'doc', 'docx');
      if ($cni && !in_array($cni->guessExtension(), $extensions)) {
        $this->addFlash('warning', ' Fichier CNI invalide');
        $fileValid = false;
      } 
      if ($fileValid) {
        if ($cni) {
          $fileName = $fileUploader->upload($cni, 'cni');
          $inscription->setCni($fileName);
        }

        $em->persist($inscription);
        $em->flush();

        $success = true;
        $mails->sendMails($inscription->getEmail(),"cecosdaformation@gmail.com","Nouvelle inscription",$inscription);
        //$this->sendMailToUser($inscription);
        //$this->sendMailToAdmin($inscription, array("cecosdaformation@gmail.com", "infos@cecosdaformation.com"));
      }
      $this->addFlash('email', 'L\'inscription a été enregistrée avec succès.');
      return $this->redirectToRoute('inscriptionCQPenpresentiel');
    }
    return $this->render('default/inscriptioncqppresentiel.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
      'success' => $success,
      'formpresentiel' => $form->createView()
    ]);
  }
      
  /**
   *  @Route("/{_locale}/formations-assistanat",
    * defaults={
    *       "_locale": "fr",
    *   },
    *   requirements={
    *       "_locale": "en|fr",
    *   }, name="formations_assistanat")
    * @param Request $request
    * @param $_locale
    * @return Response
    */
    public function formationsassistanatAction(Request $request, $_locale) {
      //Initialisation de la langue à celle choisie par le user
      $request->setLocale($_locale);
      $routeName = $request->attributes->get('_route');
    return $this->render('default/formations_assistanat.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      'currentroute' => $routeName,
    ]);
  }

  /**
    * @Route(
    *   "/{locale}/contact-form", 
    *   defaults={
    *       "locale": "fr",
    *   },
    *   requirements={
    *       "locale": "en|fr",
    *   },
    *   name="contact_mail"
    * )
    */
    public function contactMailAction(Request $request, $locale){
    
      $mail = new Emails();
      
      $nom = $_POST['name'];
      $email = $_POST['email'];
      $sujet = $_POST['subject'];
      $contenu = $_POST['message'];

      $message = \Swift_Message::newInstance()
              ->setFrom('infos@cecosdaformation.com', "CECOSDAFormation - En savoir plus")
              ->setTo(array($email, 'infos@cecosdaformation.com'))
              ->setCharset('utf-8')
              ->setContentType('text/html')
              ->setSubject('[En savoir plus] '.$sujet)
              ->setBody('Nom: '.$nom.'<br>Email: '.$email.'<br>Contenu du message: '.$contenu)
              //->attach($attachment)
      ;
      $this->get('mailer')->send($message);
      $this->addFlash('success', "Votre demande a été envoyé. Consultez votre adresse e-mail pour recevoir la réponse !");
      
    
        $mail->setEmailDestinataire($email);
        $mail->setObjectMail($sujet);
        $mail->setContent($contenu);
        $em = $this->getDoctrine()->getManager();
        $em->persist($mail);
        $em->flush();
        
      return $this->redirectToRoute('homepage', array(
                      'locale' => $locale,
                      'contact_mail' => 'yes'
                  ));
  }
}
