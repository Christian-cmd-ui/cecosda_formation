<?php

namespace AppBundle\Repository;

/**
 * SearchIndexRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SearchIndexRepository extends \Doctrine\ORM\EntityRepository {
  public function search($search) {

    $query = $this->createQueryBuilder('p')
      ->where('p.searchIndex LIKE :search')
      ->orWhere('p.description LIKE :search')
      ->orWhere('p.titre LIKE :search')
      ->setParameter('search', '%' . $search . '%')
      ->getQuery();
    return $query->getResult();
  }
}
