<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SearchIndex
 *
 * @ORM\Table(name="search_index")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SearchIndexRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SearchIndex {
  use Timestamps;
  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="titre", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $titre;

  /**
   *  @var string
   *
   * @ORM\Column(name="description", type="text")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $description;

  /**
   * @var string
   *
   * @ORM\Column(name="search_index", type="text")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $searchIndex;

  /**
   *  @var string
   *
   * @ORM\Column(name="href", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $href;

  /**
   *  @var string
   *
   * @ORM\Column(name="category", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $category;

  /**
   * Get id
   *
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set titre
   *
   * @param string $titre
   *
   * @return SearchIndex
   */
  public function setTitre($titre) {
    $this->titre = $titre;

    return $this;
  }

  /**
   * Get titre
   *
   * @return string
   */
  public function getTitre() {
    return $this->titre;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return SearchIndex
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set searchIndex
   *
   * @param string $searchIndex
   *
   * @return SearchIndex
   */
  public function setSearchIndex($searchIndex) {
    $this->searchIndex = $searchIndex;

    return $this;
  }

  /**
   * Get searchIndex
   *
   * @return string
   */
  public function getSearchIndex() {
    return $this->searchIndex;
  }

  /**
   * Set href
   *
   * @param string $href
   *
   * @return SearchIndex
   */
  public function setHref($href) {
    $this->href = $href;

    return $this;
  }

  /**
   * Get href
   *
   * @return string
   */
  public function getHref() {
    return $this->href;
  }

  /**
   * Set category
   *
   * @param string $category
   *
   * @return SearchIndex
   */
  public function setCategory($category) {
    $this->category = $category;

    return $this;
  }

  /**
   * Get category
   *
   * @return string
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @return string
   */
  public function __toString() {
    return $this->titre;
  }

  public function toArray() {
    return array(
      'title' => $this->titre,
      'description' => substr($this->description, 0, 45) . ' ... ',
      //'href' => strpos($this->href, 'http') !== false ? $this->href : 'http://localhost:8009/' . $this->href,
      'href' => strpos($this->href, 'http') !== false ? $this->href : 'https://www.cecosdaformation.com/web/' . $this->href,
    );
  }
}
