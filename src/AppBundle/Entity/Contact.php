<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Contact {
  use Timestamps;
  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $nom;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="objet", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $objet;

  /**
   * @var string
   *
   * @ORM\Column(name="message", type="text")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $message;

  /**
   * Get id
   *
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return Contact
   */
  public function setNom($nom) {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom() {
    return $this->nom;
  }

  /**
   * Set nom
   *
   * @param string $email
   *
   * @return Contact
   */
  public function setEmail($email) {
    $this->email = $email;

    return $this;
  }

  /**
   * Get email
   *
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Set objet
   *
   * @param string $objet
   *
   * @return Contact
   */
  public function setObjet($objet) {
    $this->objet = $objet;

    return $this;
  }

  /**
   * Get objet
   *
   * @return string
   */
  public function getObjet() {
    return $this->objet;
  }

  /**
   * Set message
   *
   * @param string $message
   *
   * @return Contact
   */
  public function setMessage($message) {
    $this->message = $message;

    return $this;
  }

  /**
   * Get message
   *
   * @return string
   */
  public function getMessage() {
    return $this->message;
  }

}
