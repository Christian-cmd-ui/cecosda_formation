<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inscription
 *
 * @ORM\Table(name="rejoindre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RejoindreRepository")
 * @UniqueEntity(fields={"email", "telephone"}, message="Un pré-inscription a déjà ce téléphone ou cet email")
 * @ORM\HasLifecycleCallbacks()
 */
class Rejoindre {
  use Timestamps;
  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom_prenom", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $nomPrenom;

  /**
   * @var string
   *
   * @ORM\Column(name="person_contact", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $personContact;

  /**
   * @var string
   *
   * @ORM\Column(name="ville", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $ville;

  /**
   * @var string
   *
   * @ORM\Column(name="pays", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $pays;

  /**
   * @var string
   *
   * @ORM\Column(name="sexe", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $sexe;

  /**
   * @var string
   *
   * @ORM\Column(name="type", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $type;

  /**
   * @var string
   *
   * @ORM\Column(name="telephone", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $telephone;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $email;

  /**
   * @var string
   *
   * @Assert\NotNull(message="Ce champ est obligatoire")
   * @Assert\File(
   *     maxSize = "20M",
   *     maxSizeMessage = "Fichier trop grand"
   * )
   * @ORM\Column(name="motivation", type="string")
   */
  private $motivation;

  /**
   * @var string
   *
   * @Assert\NotNull(message="Ce champ est obligatoire")
   * @Assert\File(
   *     maxSize = "20M",
   *     maxSizeMessage = "Fichier trop grand"
   * )
   * @ORM\Column(name="cv", type="string", nullable=true)
   */
  private $cv;

  /**
   * @var string
   *
   * @Assert\File(
   *     maxSize = "20M",
   *     maxSizeMessage = "Fichier trop grand"
   * )
   * @ORM\Column(name="dernier_diplome", type="string", nullable=true)
   */
  private $dernierDiplome;


  /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Filiere", inversedBy="inscriptions")
   */
  private $filiere;



  /**
   * Get id
   *
   * @return int
   */
  public function getId() {
    return $this->id;
  }



  /**
   * Set nomPrenom
   *
   * @param string $nomPrenom
   *
   * @return Inscription
   */
  public function setNomPrenom($nomPrenom) {
    $this->nomPrenom = $nomPrenom;

    return $this;
  }


  /**
   * Get nomPrenom
   *
   * @return string
   */
  public function getNomPrenom() {
    return $this->nomPrenom;
  }


  /**
   * Set cv
   *
   * @param string $cv
   *
   * @return Inscription
   */
  public function setCv($cv) {
    $this->cv = $cv;

    return $this;
  }

  /**
   * Get cv
   *
   * @return string
   */
  public function getCv() {
    return $this->cv;
  }

  /**
   * Set personContact
   *
   * @param string $personContact
   *
   * @return Inscription
   */
  public function setPersonContact($personContact) {
    $this->personContact = $personContact;

    return $this;
  }

  /**
   * Get personContact
   *
   * @return string
   */
  public function getPersonContact() {
    return $this->personContact;
  }

  /**
   * Set telephone
   *
   * @param string $telephone
   *
   * @return Inscription
   */
  public function setTelephone($telephone) {
    $this->telephone = $telephone;

    return $this;
  }

  /**
   * Get telephone
   *
   * @return string
   */
  public function getTelephone() {
    return $this->telephone;
  }

  /**
   * Set email
   *
   * @param string $email
   *
   * @return Inscription
   */
  public function setEmail($email) {
    $this->email = $email;

    return $this;
  }

  /**
   * Get email
   *
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Set motivation
   *
   * @param string $motivation
   *
   * @return Rejoindre
   */
  public function setMotivation($motivation) {
    $this->motivation = $motivation;

    return $this;
  }

  /**
   * Get motivation
   *
   * @return string
   */
  public function getMotivation() {
    return $this->motivation;
  }

  /**
   * Set ville
   *
   * @param string $ville
   *
   * @return Inscription
   */
  public function setVille($ville) {
    $this->ville = $ville;

    return $this;
  }

  /**
   * Get ville
   *
   * @return string
   */
  public function getVille() {
    return $this->ville;
  }

  /**
   * Set pays
   *
   * @param string $pays
   *
   * @return Inscription
   */
  public function setPays($pays) {
    $this->pays = $pays;

    return $this;
  }

  /**
   * Get pays
   *
   * @return string
   */
  public function getPays() {
    return $this->pays;
  }

  /**
   * Set sexe
   *
   * @param string $sexe
   *
   * @return Inscription
   */
  public function setSexe($sexe) {
    $this->sexe = $sexe;

    return $this;
  }

  /**
   * Get sexe
   *
   * @return string
   */
  public function getSexe() {
    return $this->sexe;
  }

  /**
   * Set type
   *
   * @param string $type
   *
   * @return Inscription
   */
  public function setType($type) {
    $this->type = $type;

    return $this;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get dernierDiplome
   *
   * @return string
   */
  public function getDernierDiplome() {
    return $this->dernierDiplome;
  }

  /**
   * Set dernierDiplome
   *
   * @param string $dernierDiplome
   *
   * @return Inscription
   */
  public function setDernierDiplome($dernierDiplome) {
    $this->dernierDiplome = $dernierDiplome;

    return $this;
  }

  /**
   * Get filiere
   *
   * @return Filiere
   */
  public function getFiliere() {
    return $this->filiere;
  }

  /**
   * Set filiere
   *
   * @param string $filiere
   *
   * @return Inscription
   */
  public function setFiliere($filiere) {
    $this->filiere = $filiere;

    return $this;
  }


}
