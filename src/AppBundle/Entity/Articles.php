<?php
// src/AppBundle/Entity/Product.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="Articles")
 */
class Articles {
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  /**
   * @ORM\Column(type="string", length=100)
   */
  private $titre;
  /**
   * @ORM\Column(type="string", length=200)
   */
  private $description;

  /**
   * @ORM\Column(type="string")
   */
  private $auteur;

   /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categories", inversedBy="article")
   * 
   */ 
  private $categorie;


  /**
   * @ORM\Column(type="string", nullable=true)
   */
  private $image;
  /**
   * @ORM\Column(type="text")
   */
  private $contenu;
  /**
   * @ORM\Column(type="date")
   */
  private $datepublication;
  /**
   * @ORM\Column(type="date")
   */
  private $datemiseajour;
  /**
   * @var boolean
   *
   * @ORM\Column(type="boolean", nullable=false)
   */
  private $estpublie;
  /**
   * @var boolean
   *
   * @ORM\Column(type="string", nullable=false)
   */
  private $langue;

  public function setId($id) {

    $this->id = $id;
  }

     /**
     * Get categorie
     *
     * @return Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

      /**
     * Set categorie
     *
     * @param Categories $categorie
     *
     * @return Articles
     */
    public function setCategorie(Categories $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

  public function setLangue($langue) {

    $this->langue = $langue;
  }

  public function setDescription($description) {

    $this->description = $description;
  }

  public function setAuteur($auteur) {

    $this->auteur = $auteur;
  }

  public function getId() {

    return $this->id;

  }

  public function getLangue() {

    return $this->langue;

  }

  public function getAuteur() {

      return $this->auteur;

  }

  public function getDescription() {

    return $this->description;

  }

  public function setTitre($titre) {

    $this->titre = $titre;
  }

  public function getTitre() {

    return $this->titre;

  }

   public function setImage($image) {

    $this->image = $image;
  }

  public function getImage() {

    return $this->image;

  }

  public function setContenu($contenu) {

    $this->contenu = $contenu;
  }

  public function getContenu() {

    return $this->contenu;

  }

  public function setDatepublication($datepublication) {

    $this->datepublication = $datepublication;
  }

  public function getDatepublication() {

    return $this->datepublication;

  }

  public function setDatemiseajour($datemiseajour) {

    $this->datemiseajour = $datemiseajour;
  }

  public function getDatemiseajour() {

    return $this->datemiseajour;

  }

  public function setEstpublie($estpublie) {

    $this->estpublie = $estpublie;
  }

  public function getEstpublie() {

    return $this->estpublie;

  }
}

?>