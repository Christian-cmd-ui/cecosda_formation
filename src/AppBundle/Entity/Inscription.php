<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inscription
 *
 * @ORM\Table(name="inscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InscriptionRepository")
 * @UniqueEntity(fields={"email", "telephone"}, message="Un pré-inscription a déjà ce téléphone ou cet email")
 * @ORM\HasLifecycleCallbacks()
 */
class Inscription {
  use Timestamps;
  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom_prenom", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $nomPrenom;

  /**
   * @var string
   *
   * @ORM\Column(name="langue", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $langue;

  /**
   * @var string
   *
   * @ORM\Column(name="sexe", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $sexe;

  /**
   * @var string
   *
   * @ORM\Column(name="type", type="string", nullable=true)
   * En ligne ou en presentiel
   */
  private $type;

  /**
   * @var string
   *
   * @ORM\Column(name="telephone", type="string",nullable=true)
   * 
   */
  private $telephone;
  
  /**
   * @var string
   *
   * @ORM\Column(name="telephoneparent1", type="string", nullable=true)
   * 
   * Ajout pour spécial informatique pour avoir une table unique
   */
  private $telephoneparent1;
  
  /**
   * @var string
   *
   * @ORM\Column(name="telephoneparent2", type="string", nullable=true)
   * Ajout pour spécial informatique pour avoir une table unique
   * 
   */
  private $telephoneparent2;
  
  /**
   * @var string
   *
   * @ORM\Column(name="plagehoraire", type="string", nullable=true)
   * Ajout pour spécial informatique pour avoir une table unique
   * 
   */
  private $plagechoisie;
  /**
   * @var string
   *
   * @ORM\Column(name="specialite", type="string", nullable=true)
   * Ajout pour spécial informatique pour avoir une table unique
   * Pour les adultes et les etudiants de SPI
   */
  private $specialite;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $email;

  /**
   * @var string
   *
   * 
   * @Assert\File(
   *     maxSize = "20M",
   *     maxSizeMessage = "Fichier trop grand"
   * )
   * @ORM\Column(name="cni", type="string", nullable=true)
   */
  private $cni;


  /**
   * @var string
   * 
   * @ORM\Column(name="dernier_diplome", type="string", nullable=true)
   */
  private $dernierDiplome;

  /**
   * @var string
   * 
   * @ORM\Column(name="classe", type="string", nullable=true)
   */
  private $classe;
  
  
  /**
   * @var string
   * 
   * @ORM\Column(name="etablissement", type="string", nullable=true)
   */
  private $etablissement;
  
  /**
   * @var string
   * 
   * @ORM\Column(name="occupation", type="string", nullable=true)
   */
  private $occupation;

  /**
   * @var bool
   *
   * @ORM\Column(name="equipement", type="boolean", nullable=true)
   */
  private $equipement;

  /**
   * @var bool
   *
   * @ORM\Column(name="internet", type="boolean", nullable=true)
   */
  private $Internet;

  /**
   * @var bool
   *
   * @ORM\Column(name="phone", type="boolean", nullable=true)
   */
  private $smartphone;
  
/**
   * @var bool
   *
   * @ORM\Column(name="connect", type="boolean", nullable=true)
   */
  private $connect;

 

  /**
   * Get the value of id
   */
  public function getId()
  {
    return $this->id;
  }



  /**
   * Get the value of nomPrenom
   */
  public function getNomPrenom()
  {
    return $this->nomPrenom;
  }

  /**
   * Set the value of nomPrenom
   */
  public function setNomPrenom( $nomPrenom)
  {
    $this->nomPrenom = $nomPrenom;

    return $this;
  }

  /**
   * Get the value of langue
   */
  public function getLangue()
  {
    return $this->langue;
  }

  /**
   * Set the value of langue
   */
  public function setLangue( $langue)
  {
    $this->langue = $langue;

    return $this;
  }

  /**
   * Get the value of sexe
   */
  public function getSexe()
  {
    return $this->sexe;
  }

  /**
   * Set the value of sexe
   */
  public function setSexe( $sexe)
  {
    $this->sexe = $sexe;

    return $this;
  }

  /**
   * Get the value of type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set the value of type
   */
  public function setType( $type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Get the value of telephone
   */
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
   * Set the value of telephone
   */
  public function setTelephone( $telephone)
  {
    $this->telephone = $telephone;

    return $this;
  }

  /**
   * Get the value of email
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set the value of email
   */
  public function setEmail( $email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get the value of cni
   */
  public function getCni()
  {
    return $this->cni;
  }

  /**
   * Set the value of cni
   */
  public function setCni( $cni)
  {
    $this->cni = $cni;

    return $this;
  }

  /**
   * Get the value of dernierDiplome
   */
  public function getDernierDiplome()
  {
    return $this->dernierDiplome;
  }

  /**
   * Set the value of dernierDiplome
   */
  public function setDernierDiplome( $dernierDiplome)
  {
    $this->dernierDiplome = $dernierDiplome;

    return $this;
  }

  /**
   * Get the value of connect
   */
  public function isConnect()
  {
    return $this->connect;
  }

  /**
   * Set the value of connect
   */
  public function setConnect( $connect)
  {
    $this->connect = $connect;

    return $this;
  }
  
  

  /**
   * Set the value of id
   */
  public function setId( $id)
  {
    $this->id = $id;

    return $this;
  }


  /**
   * Get the value of telephoneparent1
   */
  public function getTelephoneparent1()
  {
    return $this->telephoneparent1;
  }

  /**
   * Set the value of telephoneparent1
   */
  public function setTelephoneparent1( $telephoneparent1)
  {
    $this->telephoneparent1 = $telephoneparent1;

    return $this;
  }

  /**
   * Get the value of telephoneparent2
   */
  public function getTelephoneparent2()
  {
    return $this->telephoneparent2;
  }

  /**
   * Set the value of telephoneparent2
   */
  public function setTelephoneparent2( $telephoneparent2)
  {
    $this->telephoneparent2 = $telephoneparent2;

    return $this;
  }

  /**
   * Get the value of plagechoisie
   */
  public function getPlagechoisie()
  {
    return $this->plagechoisie;
  }

  /**
   * Set the value of plagechoisie
   */
  public function setPlagechoisie( $plagechoisie)
  {
    $this->plagechoisie = $plagechoisie;

    return $this;
  }

  /**
   * Get the value of specialite
   */
  public function getSpecialite()
  {
    return $this->specialite;
  }

  /**
   * Set the value of specialite
   */
  public function setSpecialite( $specialite)
  {
    $this->specialite = $specialite;

    return $this;
  }



  /**
   * Get the value of classe
   */
  public function getClasse()
  {
    return $this->classe;
  }

  /**
   * Set the value of classe
   */
  public function setClasse( $classe)
  {
    $this->classe = $classe;

    return $this;
  }

  /**
   * Get the value of etablissement
   */
  public function getEtablissement()
  {
    return $this->etablissement;
  }

  /**
   * Set the value of etablissement
   */
  public function setEtablissement( $etablissement)
  {
    $this->etablissement = $etablissement;

    return $this;
  }

  /**
   * Get the value of occupation
   */
  public function getOccupation()
  {
    return $this->occupation;
  }

  /**
   * Set the value of occupation
   */
  public function setOccupation( $occupation)
  {
    $this->occupation = $occupation;

    return $this;
  }

  /**
   * Get the value of smartphone
   */
  public function isSmartphone()
  {
    return $this->smartphone;
  }

  /**
   * Set the value of smartphone
   */
  public function setSmartphone($smartphone)
  {
    $this->smartphone = $smartphone;

    return $this;
  }

  /**
   * Get the value of Internet
   */
  public function isInternet()
  {
    return $this->Internet;
  }

  /**
   * Set the value of Internet
   */
  public function setInternet( $Internet)
  {
    $this->Internet = $Internet;

    return $this;
  }

  /**
   * Get the value of equipement
   */
  public function isEquipement()
  {
    return $this->equipement;
  }

  /**
   * Set the value of equipement
   */
  public function setEquipement($equipement)
  {
    $this->equipement = $equipement;

    return $this;
  }
}
