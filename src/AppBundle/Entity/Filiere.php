<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Filiere
 *
 * @ORM\Table(name="filiere")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FiliereRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Filiere
{
    use Timestamps;
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $libelle;

  /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $mode;

     /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Inscription", mappedBy="filiere")
     */
    private $inscriptions;


    /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Formation", inversedBy="filiere")
   */
  private $formation;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
  
    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Filiere
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    
    /**
     * Set description
     *
     * @param string $description
     *
     * @return Filiere
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    
    /**
     * Set formation
     *
     * @param Formation $formation
     *
     * @return Filiere
     */
    public function setFormation(Formation $formation)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Get formation
     *
     * @return Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }



    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Filiere
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return mixed
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->libelle;
    }

}

