<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inscription
 *
 * @ORM\Table(name="inscriptionprepa")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InscriptionprepaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Inscriptionprepa {
  use Timestamps;
  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom_prenom", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $nomenfant;

  /**
   * @var string
   *
   * @ORM\Column(name="person_contact", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $nomparent;

  /**
   * @var string
   *
   * @ORM\Column(name="ville", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $ville;


  /**
   * @var string
   *
   * @ORM\Column(name="langue", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $langue;

   /**
   * @var string
   *
   * @ORM\Column(name="sexe", type="string", nullable=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $sexe;

  /**
   * @var string
   *
   * @ORM\Column(name="type", type="string", nullable=true)
   * pendant les classes, les grandes vacances ou les conges
   */
  private $type;

  /**
   * @var string
   *
   * @ORM\Column(name="dureeformation", type="string", nullable=true)
   * par mois
   */
  private $dureeformation;

  /**
   * @var string
   *
   * @ORM\Column(name="telephone", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $telephone;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="formation", type="string")
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   */
  private $formation;

 

  /**
   * @var string
   *
   * @ORM\Column(name="classe", type="string")
   */
  private $classe;




  /**
   * Get the value of classe
   *
   * @return  string
   */ 
  public function getClasse()
  {
    return $this->classe;
  }

  /**
   * Set the value of classe
   *
   * @param  string  $classe
   *
   * @return  self
   */ 
  public function setClasse($classe)
  {
    $this->classe = $classe;

    return $this;
  }

  /**
   * Get the value of email
   *
   * @return  string
   */ 
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set the value of email
   *
   * @param  string  $email
   *
   * @return  self
   */ 
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get the value of telephone
   *
   * @return  string
   */ 
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
   * Set the value of telephone
   *
   * @param  string  $telephone
   *
   * @return  self
   */ 
  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;

    return $this;
  }

  /**
   * Get the value of dureeformation
   *
   * @return  string
   */ 
  public function getDureeformation()
  {
    return $this->dureeformation;
  }

  /**
   * Set the value of dureeformation
   *
   * @param  string  $dureeformation
   *
   * @return  self
   */ 
  public function setDureeformation($dureeformation)
  {
    $this->dureeformation = $dureeformation;

    return $this;
  }

  /**
   * Get the value of type
   *
   * @return  string
   */ 
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set the value of type
   *
   * @param  string  $type
   *
   * @return  self
   */ 
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Get the value of sexe
   *
   * @return  string
   */ 
  public function getSexe()
  {
    return $this->sexe;
  }

  /**
   * Set the value of sexe
   *
   * @param  string  $sexe
   *
   * @return  self
   */ 
  public function setSexe($sexe)
  {
    $this->sexe = $sexe;

    return $this;
  }

  /**
   * Get the value of langue
   *
   * @return  string
   */ 
  public function getLangue()
  {
    return $this->langue;
  }

  /**
   * Set the value of langue
   *
   * @param  string  $langue
   *
   * @return  self
   */ 
  public function setLangue($langue)
  {
    $this->langue = $langue;

    return $this;
  }

  /**
   * Get the value of ville
   *
   * @return  string
   */ 
  public function getVille()
  {
    return $this->ville;
  }

  /**
   * Set the value of ville
   *
   * @param  string  $ville
   *
   * @return  self
   */ 
  public function setVille($ville)
  {
    $this->ville = $ville;

    return $this;
  }

  /**
   * Get the value of nomparent
   *
   * @return  string
   */ 
  public function getNomparent()
  {
    return $this->nomparent;
  }

  /**
   * Set the value of nomparent
   *
   * @param  string  $nomparent
   *
   * @return  self
   */ 
  public function setNomparent($nomparent)
  {
    $this->nomparent = $nomparent;

    return $this;
  }

  /**
   * Get the value of nomenfant
   *
   * @return  string
   */ 
  public function getNomenfant()
  {
    return $this->nomenfant;
  }

  /**
   * Set the value of nomenfant
   *
   * @param  string  $nomenfant
   *
   * @return  self
   */ 
  public function setNomenfant($nomenfant)
  {
    $this->nomenfant = $nomenfant;

    return $this;
  }

  /**
   * Get the value of id
   *
   * @return  int
   */ 
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set the value of id
   *
   * @param  int  $id
   *
   * @return  self
   */ 
  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  /**
   * Get the value of formation
   *
   * @return  string
   */ 
  public function getFormation()
  {
    return $this->formation;
  }

  /**
   * Set the value of formation
   *
   * @param  string  $formation
   *
   * @return  self
   */ 
  public function setFormation(string $formation)
  {
    $this->formation = $formation;

    return $this;
  }
}
