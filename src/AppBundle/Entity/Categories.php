<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Categories
 * 
 *
 * @ORM\Table(name="Categories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriesRepository")
 * @UniqueEntity(fields={"nom", "langue"}, message="Un Categorie existe déjà dans cette langue")
 * @ORM\HasLifecycleCallbacks()
 */

class Categories
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $nom;

    /**
     * @var bool
     * @ORM\Column(name="langue", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $langue;
    
   /**
   * @ORM\OneToMany(targetEntity="AppBundle\Entity\Articles", mappedBy="categorie")
   */
   private $article;

    
    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categories
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set langue
     *
     * @param boolean $langue
     *
     * @return Categories
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }
}

