<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Formation
 *
 * @ORM\Table(name="Formation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Formation
{
    use Timestamps;
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $libelle;

     /**
     * @var int
     *
     * 
     * @ORM\Column(name="duree", type="integer")
     * 
     */
    private $duree;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Inscription", mappedBy="formation")
     */
    private $inscriptions;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Filiere", mappedBy="formation")
     */
    private $filiere;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
  
    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Formation
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }

 
    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Formation
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return mixed
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }


    /**
     * @return mixed
     */
    public function getFiliere()
    {
        return $this->filiere;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->libelle;
    }

}

