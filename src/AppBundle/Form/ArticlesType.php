<?php

namespace AppBundle\Form;

use AppBundle\Entity\Articles;
use AppBundle\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class ArticlesType
 *
 * @package AppBundle\Form
 */
class ArticlesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => 'Titre de votre article',
                'attr' => array(
                          'placeholder' => 'Titre de votre article',
                          'required' => 'required',
                        ),
            ])
            ->add('description', TextType::class, [
                'label' => 'Faites un bref resume de votre article ici',
                'attr' => array(
                          'placeholder' => 'Bref resume de votre article',
                          'required' => 'required',
                        ),
            ])
            ->add('auteur', TextType::class, [
                'label' => 'Nom de l\'auteur',
                'attr' => array(
                          'placeholder' => 'Auteur de l\' article',
                          'required' => 'required',
                        ),
            ])

            ->add('contenu', TextareaType::class, [
                            'label' => 'Contenu de votre article'

                        ])
   
      
            ->add('estpublie', ChoiceType::class, [
                'choices'  => [
                        'Yes' => true,
                        'No' => false,
                    ],
            ])
      
            ->add('categorie', EntityType::class, [
                'label' => 'Catégories d\'articles',
                'class' => Categories::class,
              ])
            ->add('langue', ChoiceType::class, [
                'choices'  => [
                        'Français' => '1',
                        'Anglais' => '0',
                    ],
            ])
            
            
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Articles::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_articles';
    }
}
