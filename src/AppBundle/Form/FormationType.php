<?php

namespace AppBundle\Form;

use AppBundle\Entity\Formation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FormationType
 * @package AppBundle\Form
 */
class FormationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
            'label' => 'Nom de la formation'])

            ->add('duree', ChoiceType::class, [
                'choices'  => [
                    '01mois' => '01 mois',
                    '03mois' => '03 mois',
                    '06mois' => '06 mois',
                    '09mois' => '09 mois',
                ],
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Formation::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_Formation';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_Formation';
    }
}
