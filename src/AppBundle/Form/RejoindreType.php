<?php

namespace AppBundle\Form;

use AppBundle\Entity\Filiere;
use AppBundle\Entity\Rejoindre;
use AppBundle\Entity\OptionApprentissage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InscriptionType
 * @package AppBundle\Form
 */
class RejoindreType extends AbstractType {
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('nomPrenom', TextType::class)
      ->add('personContact', TextType::class)
      ->add('telephone', TextType::class)
      ->add('email', TextType::class)
      ->add('ville', TextType::class)
      ->add('pays', TextType::class)
      ->add('sexe', ChoiceType::class, [
        'choices' => [
          'M' => "M",
          'F' => 'F',
        ],
      ])
      ->add('type', ChoiceType::class, [
        'choices' => [
          'Cour du jour|Morning' => "Morning|Cour du jour",
          'Cour du soir|night'   =>   'Night|Cour du soir',
        ],
      ])
      ->add('cv', FileType::class, [
        
        'required' => false,
      ])
      ->add('motivation', FileType::class, [
        'required' => false,
      ])
      ->add('dernierDiplome', FileType::class, [
        'required' => false,
      ])

    
      ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => Rejoindre::class,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix() {
    return 'app_bundle_rejoindre';
  }

  /**
   * @return string
   */
  public function getName() {
    return 'app_bundle_rejoindre';
  }
}
