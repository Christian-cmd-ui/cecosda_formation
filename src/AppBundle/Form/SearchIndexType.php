<?php

namespace AppBundle\Form;

use AppBundle\Entity\SearchIndex;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchIndexType
 * @package AppBundle\Form
 */
class SearchIndexType extends AbstractType {
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('titre', TextType::class, [
        'label' => 'Titre de l\'index',
      ])->add('description', TextareaType::class, [
      'label' => 'Description de l\'index',
      'attr' => ['rows' => 10],
    ])->add('searchIndex', TextareaType::class, [
      'label' => 'Contenu de l\'index',
      'attr' => ['rows' => 10],
    ])
      ->add('href', TextType::class, [
        'label' => 'Lien de l\'index',
      ])->add('category', ChoiceType::class, [
      'label' => 'Categorie de l\'index',
      'choices' => [
        'Page' => 'page',
        'Article' => 'article',
        'Autre' => 'autre',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => SearchIndex::class,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix() {
    return 'app_bundle_search_index';
  }

  /**
   * @return string
   */
  public function getName() {
    return 'app_bundle_search_index';
  }
}
