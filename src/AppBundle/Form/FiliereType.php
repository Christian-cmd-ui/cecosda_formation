<?php

namespace AppBundle\Form;

use AppBundle\Entity\Filiere;
use AppBundle\Entity\Formation;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FiliereType
 * @package AppBundle\Form
 */
class FiliereType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
            'label' => 'Nom de la filière'])

            ->add('mode', ChoiceType::class, [
                'choices'  => [
                    'En ligne' => 'En ligne',
                    'En salle' => 'En salle', 
                    'En ligne et en salle' => 'En ligne et en salle',
                ],
            ])

            ->add('formation', EntityType::class, [
                'label' => 'Type de formation',
                'class' => Formation::class,
              ])
            ->add('description', TextType::class, [
                'label' => 'Description de la filière']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Filiere::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_filiere';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_filiere';
    }
}
