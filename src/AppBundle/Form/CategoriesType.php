<?php

namespace AppBundle\Form;

use AppBundle\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CategoriesType
 * @package AppBundle\Form
 */
class CategoriesType extends AbstractType {
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('nom', TextType::class, [
        'attr' => array(
          'placeholder' => 'Le nom de la nouvelle catégorie',
          'required' => 'required',
        ),
      ])
      ->add('langue', ChoiceType::class, [
        'choices'  => [
                'Français' => "fr",
                'Anglais' => "en",
            ],
    ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => Categories::class,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix() {
    return 'app_bundle_Categories';
  }

  /**
   * @return string
   */
  public function getName() {
    return 'app_bundle_Categories';
  }
}
