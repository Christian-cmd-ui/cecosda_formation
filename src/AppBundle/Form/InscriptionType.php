<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class InscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nomPrenom')
        ->add('langue', ChoiceType::class, [
            'choices' => [
                'FR' => 'FR',
                'EN' => 'EN',
                'FR|EN' => 'FR|EN',
              ]])
        ->add('sexe', ChoiceType::class, [
            'choices' => [
                'M' => 'M',
                'F' => 'F',
              ]])
        ->add('type')
        ->add('classe')
        ->add('etablissement')
        ->add('occupation')
        ->add('telephone')
        ->add('telephoneparent1')
        ->add('telephoneparent2')
        ->add('plagechoisie')
        ->add('specialite')
        ->add('email')
        ->add('cni')
        ->add('dernierDiplome')
        ->add('equipement')
        ->add('Internet')
        ->add('smartphone')
        ->add('connect');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Inscription'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_inscription';
    }


}
