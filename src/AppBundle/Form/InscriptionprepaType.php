<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InscriptionprepaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomenfant', TextType::class, [
            'label' => 'Nom et prénom',
            'attr' => array(
              'placeholder' => 'Nom et prénom',
            ),
          ])
        ->add('ville', TextType::class, [
            'label' => 'Ville',
            'attr' => array(
              'placeholder' => 'Ville',
            ),
          ])
        ->add('nomparent', TextType::class, [
            'label' => 'Nom du parent/Tuteur',
            'attr' => array(
              'placeholder' => 'Parent/Tuteur',
            ),
          ])
        ->add('type', ChoiceType::class, [
            'label' => 'Formation choisie',
            'attr' => array(
              'placeholder' => 'Choisissez ici',
            ),
            'choices' => [
                'Développement Web' => 'Développement Web',
                'Infographie' => 'Infographie',
                'Bureautique' => 'Bureautique',
                'Vente en ligne' => 'Vente en ligne',
                'Logiciels d\'infographie' => 'Logiciels d\'infographie',
                'Logiciels de bureautique' => 'logiciels de Bureautique',
                'Framework de développement web' => 'Framework de développement web',
                'Dessin technique' => 'Dessin technique',
                'Logiciels de gestion' => 'Logiciels de gestion',
              ],
          ])
        ->add('langue', ChoiceType::class, [
            'label' => 'Langue',
            'attr' => array(
              'placeholder' => 'Choisissez ici',
            ),
            'choices' => [
                'FR' => 'Francais',
                'EN' => 'Anglais',
                
              ],
          ])
        ->add('sexe', ChoiceType::class, [
            'label' => 'Sexe',
            'attr' => array(
              'placeholder' => 'Choisissez ici',
            ),
            'choices' => [
                'M' => 'Male',
                'F' => 'Female',
                
              ],
          ])
        ->add('telephone', TextType::class, [
            'label' => 'Numéro de téléphone',
            'attr' => array(
              'placeholder' => 'Numero',
            ),
          ])
        ->add('classe', TextType::class, [
            'label' => 'classe',
            'attr' => array(
              'placeholder' => 'classe',
            ),
          ])
        ->add('email', TextType::class, [
            'label' => 'Adresse Email',
            'attr' => array(
              'placeholder' => 'Email',
            ),
          ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Inscriptionprepa'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_inscriptionprepa';
    }


}
