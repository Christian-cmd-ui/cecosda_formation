<?php

namespace AppBundle\Form;

use AppBundle\Entity\OptionApprentissage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OptionApprentissageType
 * @package AppBundle\Form
 */
class OptionApprentissageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
            'label' => 'Nom de l\'option d\'apprentissage'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OptionApprentissage::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_option_apprentissage';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_option_apprentissage';
    }
}
