<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactType
 * @package AppBundle\Form
 */
class ContactType extends AbstractType {
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('nom', TextType::class, [
        'attr' => array(
          'placeholder' => 'Votre nom',
          'required' => 'required',
        ),
      ])
      ->add('objet', TextType::class, [
        'attr' => array(
          'placeholder' => 'Objet',
          'required' => 'required',
        ),
      ])
      ->add('email', TextType::class, [
        'attr' => array(
          'placeholder' => 'Votre email',
          'required' => 'required',
        ),
      ])
      ->add('message', TextareaType::class, [
        'attr' => array(
          'placeholder' => 'Votre message',
          'required' => 'required',
        ),
      ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => Contact::class,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix() {
    return 'app_bundle_contact';
  }

  /**
   * @return string
   */
  public function getName() {
    return 'app_bundle_contact';
  }
}
