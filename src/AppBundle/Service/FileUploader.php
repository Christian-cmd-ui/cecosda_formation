<?php

// AppBundle/Service/FileUploader.php
namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileUploader
 * @package AppBundle\Service
 */
class FileUploader
{
    /**
     * @var
     */
    private $targetDir;

    /**
     * FileUploader constructor.
     * @param $targetDir
     */
    public function __construct($targetDir = null)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param UploadedFile $file
     * @param string $directoryName
     * @return string
     */
    public function upload(UploadedFile $file, $directoryName = null)
    {
        $fileName = 'files/'.$directoryName.'/'.md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDir().'/'.$directoryName, $fileName);

        return $fileName;
    }

    /**
     * @param $img
     * @return string
     */
    public function uploadEvaluation($img)
    {
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = 'files/evaluations/'.uniqid() . '.png';
        file_put_contents($file, $data);
        return $file;
    }


    /**
    * @param string $fileName
    * @return void
    */
    public function removeFile($fileName)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($fileName);
    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }
}